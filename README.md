# Please contribute
This is open analysis code. All LHCb members are invited to contribute. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions.

# Requirements
A `C++14` compiler is needed!

# Documentation
The package contains several wrapper scripts that allow using ROOT by writing config files only
(based on the INFO parser of [boost::property_tree](http://www.boost.org/doc/libs/1_64_0/doc/html/property_tree/parsers.html#property_tree.parsers.info_parser)) <br>
Links to the documentation of the wrappers:
- [stuff2hist](doc/stuff2hist.md) for making ROOT histograms from TTrees, TChains or RooDataSets
- [draw_stuff](doc/draw_stuff.md) for plotting with ROOT
- [tree_trimmer](doc/tree_trimmer.md) for TTree manipulations
- [misID_beta](doc/misID_beta.md) for studying misidentification backgrounds
- [CutOptScan] (doc/CutOptScan.md) for rectangular cut optimisation
- [trainMVA] (doc/trainMVA.md) for classification or regression tasks with TMVA
- [applyMVA] (doc/applyMVA.md) for applying TMVA weight-files to ntuples

