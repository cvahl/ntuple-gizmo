#ifndef SELECTION_PIDSUBS_H 
#define SELECTION_PIDSUBS_H 1

#include <TLorentzVector.h>

// substitute PID for one daughter
TLorentzVector
pidSub(double mother_PX, double mother_PY, double mother_PZ, double mother_MM, // original momentum
       double daughter_PX, double daughter_PY, double daughter_PZ, double oldmass, double newmass)
{
  TLorentzVector psubs,olddaughter,oldmother;
  psubs.SetXYZM(daughter_PX,daughter_PY,daughter_PZ,newmass);
  olddaughter.SetXYZM(daughter_PX,daughter_PY,daughter_PZ,oldmass);
  oldmother.SetXYZM(mother_PX,mother_PY,mother_PZ,mother_MM);
  
  return oldmother-olddaughter+psubs;
}

template<class LV, class fpn>
  inline LV pidSub(const LV &mother, const LV &daughter_to_sub, const fpn &newmass){
  LV psubs;
  psubs.SetVectM(daughter_to_sub.Vect(),newmass);
  return mother-daughter_to_sub+psubs;
}

#endif // SELECTION_PIDSUBS_H
