/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  TMVA traning wrapper using boost property trees
*/
//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
//TMVA
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Config.h"
//STL
#include <vector>
#include <iostream>
#include <string>
//local
#include <../IOjuggler/IOjuggler.h>

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = parse_options(argc, argv, "c:d:o:hv:");
  MessageService msg_svc("trainMVA",static_cast<MSG_LVL>(options.get<int>("verbosity")));    
  //get ptree
  auto configtree = get_ptree(options.get<std::string>("config"));
  //Create a ROOT output file where TMVA will store ntuples, histograms, etc.
  auto wd = options.get<std::string>("workdir");
  TFile* outputFile = TFile::Open(TString::Format("%s/%s",wd.data(),options.get<std::string>("outfilename").data()),"RECREATE");

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msg_svc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  //lambda for testing and training individual tasks
  auto train_and_test = [&msg_svc,&wd,outputFile] (const pt::ptree& taskconfig, const std::string& factory_name) {

    TStopwatch task_clock;
    task_clock.Start();

    //set output directory of xml and class file
    TMVA::Tools::Instance();
    TMVA::gConfig().GetIONames().fWeightFileDir = wd;

    auto sig_filename = taskconfig.get<std::string>("sig_filename");
    auto bkg_filename = taskconfig.get<std::string>("bkg_filename");
    auto sig_treename = taskconfig.get<std::string>("sig_treename");
    auto bkg_treename = taskconfig.get<std::string>("bkg_treename");

    msg_svc.infomsg("Input files for this task: ");
    msg_svc.infomsg("sig_filename: " + sig_filename);
    msg_svc.infomsg("sig_treename: " + sig_treename);
    msg_svc.infomsg("bkg_filename: " + bkg_filename);
    msg_svc.infomsg("bkg_treename: " + bkg_treename);

    auto sig_input = get_file(sig_filename,wd);
    auto bkg_input = get_file(bkg_filename,wd);

    auto sig_tree = get_obj<TTree>(sig_input,sig_treename);
    auto bkg_tree = get_obj<TTree>(bkg_input,bkg_treename);

    //add friend tree(s) and push them into a vector because they will go out of scope
    std::vector< TFriendElement* > fes;
    if(taskconfig.get_child_optional("sig_friendfiles")){
      for(const auto& sff : taskconfig.get_child("sig_friendfiles")){
        auto ff = get_file(sff.second.get<std::string>("filename"),wd);
        fes.push_back(sig_tree->AddFriend(sff.second.get<std::string>("treename").data(),ff));
      }
    }
    if(taskconfig.get_child_optional("bkg_friendfiles")){
      for(const auto& bff : taskconfig.get_child("bkg_friendfiles")){
        auto ff = get_file(bff.second.get<std::string>("filename"),wd);
        fes.push_back(bkg_tree->AddFriend(bff.second.get<std::string>("treename").data(),ff));
      }
    }

    //Create TMVA factory which handles the training and testing
    TMVA::Factory factory(factory_name, outputFile, taskconfig.get<std::string>("factory_option"));

    //add input variables to train MVA
    for(const auto& var : taskconfig.get_child("variables"))
      factory.AddVariable(var.first, var.second.get("label",var.first), var.second.get("unit"," "),
                          static_cast<char>(var.second.get("type",'F')));

    //add spectator variables to train MVA
    for(const auto& var : taskconfig.get_child("spectators"))
      factory.AddSpectator(var.first,var.second.get("label",var.first),var.second.get("unit"," "),
                           var.second.get<double>("low"),var.second.get<double>("high"));

    //get signal and background trees, add them to the factory
    factory.AddSignalTree    (sig_tree);
    factory.AddBackgroundTree(bkg_tree);

    //use weights
    if(auto sig_weight = taskconfig.get_optional<std::string>("sig_weightname"))
      factory.SetSignalWeightExpression(*sig_weight);
    if(auto bkg_weight = taskconfig.get_optional<std::string>("bkg_weightname"))
      factory.SetBackgroundWeightExpression(*bkg_weight);

    //Prepare Testing and Training. Apply additional cuts on the signal and background samples (can be different)
    factory.PrepareTrainingAndTestTree(TCut(taskconfig.get("sig_cut","").data()),
                                       TCut(taskconfig.get("bkg_cut","").data()),
                                       taskconfig.get<std::string>("traintestopt"));

    //book TMVA methods
    for(const auto& meth : taskconfig.get_child("method"))
      factory.BookMethod(static_cast<TMVA::Types::EMVA>(meth.second.get<int>("type")),
                         meth.first+factory_name, meth.second.get<std::string>("option"));

    // Train MVAs using the set of training events
    factory.TrainAllMethods();
    // ---- Evaluate all MVAs using the set of test events
    factory.TestAllMethods();
    // ----- Evaluate and compare performance of all configured MVAs
    factory.EvaluateAllMethods();

    task_clock.Stop();
    if(msg_svc.GetMsgLvl() >= MSG_LVL::INFO)task_clock.Print();

  };

  //loop all tasks individually for training and testing
  for(const auto& task : configtree.get_child("tasks"))
    train_and_test(task.second,task.first);

  // Save the output
  outputFile->Close();
  msg_svc.infomsg(TString::Format("==> View results with:\nTMVA::TMVAGui(\"%s\")",outputFile->GetName()));
  clock.Stop();
  if(msg_svc.GetMsgLvl() >= MSG_LVL::INFO)clock.Print();

}
