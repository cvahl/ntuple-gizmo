/*!                               
 *  @file      drawPlots.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to read and draw plots from some .root input files.
 *  @return    Returns the plots in the desired format.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser
#include <vector>
#include <memory>
#include <utility>

//ROOT libraries
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TMultiGraph.h>
#include <TEfficiency.h>
#include <TPaveText.h>
#include <TAxis.h>
#include <TPaletteAxis.h>
#include <TLatex.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

#include "../include/lhcbStyle.C"

using namespace std;
namespace pt = boost::property_tree;

//function to draw TH1D
void draw_TH1D(TFile *inFile, std::string outPath, const pt::ptree Canv){

  //defining the Canvas
  TCanvas *canv = new TCanvas("canv", "",
                              Canv.get<double>("width"),
                              Canv.get<double>("height"));

  gStyle->SetOptStat(0);

  //--------------------------//
  //  optional configuration  //
  //--------------------------//

  // OPTENTRY legend
  bool drawLegend = false;
  boost::optional<bool> child_legend = Canv.get_optional<bool>("drawLegend");
  if (child_legend) // key exists
    drawLegend = child_legend.get();
  TLegend *leg = new TLegend(0.6,0.68,0.8,0.88);
  leg->SetTextSize(0.05);
  leg->SetBorderSize(0);
  int legEntries = 0;
  unsigned int maxEntryLength = 0;
  unsigned int entryLength = 0;

  // OPTENTRY legendHead
  boost::optional<string> child_legendHead = Canv.get_optional<string>("legendHead");
  if (child_legendHead) // key exists
    leg->SetHeader((child_legendHead.get()).c_str());

  // OPTENTRY legendPos
  string legendPos;
  boost::optional<string> child_legendPos = Canv.get_optional<string>("legendPos");
  if (child_legendPos) // key exists
    legendPos = child_legendPos.get();

  // OPTENTRY ylog
  bool yLogScale = false;
  boost::optional<bool> child_ylogscale = Canv.get_optional<bool>("yLogScale");

  if (child_ylogscale) // key exists
    yLogScale = child_ylogscale.get();

  if (yLogScale) canv->SetLogy();

  double xMax = 0;
  
  //-----------------------------------------------//
  //  get min and max y values for multiple plots  //
  //-----------------------------------------------//

  double yMin = 999999.;
  double yMax = -999999.;

  int entryNum=0;
  TH1D *histo_1Dy = new TH1D();

  //loop over plots of the canvas
  for(pt::ptree::const_iterator Plot = Canv.get_child("Plots").begin(); Plot != Canv.get_child("Plots").end(); ++Plot){

    histo_1Dy = (TH1D*) inFile->Get((Plot->second.get<std::string>("input_name")).c_str());
    entryNum++;
    
    // OPTENTRY normalize
    bool normalize = false;
    boost::optional<bool> child_normalize = Plot->second.get_optional<bool>("normalize");
    
    if (child_normalize) // key exists
      normalize = child_normalize.get();
    
    if (normalize)
      histo_1Dy->Scale(1./histo_1Dy->Integral());
    
    //apply scaling
    histo_1Dy->Scale(Plot->second.get<double>("scaling"));
    
    if(histo_1Dy->GetMinimum() < yMin)
      yMin = histo_1Dy->GetMinimum();
    
    if(histo_1Dy->GetMaximum() > yMax)
      yMax = histo_1Dy->GetMaximum();
    
  }  //loop over plots of the canvas
  
  delete histo_1Dy;
  
  //fix y axis range for log scale
  if(yLogScale){
    if(yMin<1)
      yMin=1;

    yMax = pow(10,1.2*(log10(yMax)-log10(yMin))+log10(yMin));

    if(yMin != 1)
      yMin = pow(10,log10(yMax)-1.2*(log10(yMax)-log10(yMin)));
  } else {
    yMin=0;
    yMax=1.2*yMax;
  }

  //------------------//
  //  draw the plots  //
  //------------------//
  
  TH1D *histo_1D = new TH1D();

  boost::optional<std::string> child_xtitle = Canv.get_optional<std::string>("x_axisTitle");
  boost::optional<std::string> child_ytitle = Canv.get_optional<std::string>("y_axisTitle");
  
  //loop over plots
  for(pt::ptree::const_iterator Plot = Canv.get_child("Plots").begin();
      Plot != Canv.get_child("Plots").end(); ++Plot){
    
    histo_1D = (TH1D*) inFile->Get((Plot->second.get<std::string>("input_name")).c_str());
    
    if(histo_1D==nullptr){
      std::cerr << "ERROR: histogram " << Plot->second.get<std::string>("input_name") << " not found in file." << std::endl;
      throw;
    }

    // hide histo titles
    histo_1D->SetTitle("");

    // OPTENTRY fillstyle
    boost::optional<unsigned int> child_SetFillStyle = Plot->second.get_optional<unsigned int>("SetFillStyle");
    if (child_SetFillStyle){ // key exists
      histo_1D->SetFillStyle(Plot->second.get<unsigned int>("SetFillStyle"));
      histo_1D->SetFillColor(Plot->second.get<unsigned int>("line_color"));
    }

    // OPTENTRY transparency
    boost::optional<double> child_transparency = Plot->second.get_optional<double>("transparency");
    if (child_transparency){ // key exists
      histo_1D->SetFillColorAlpha(Plot->second.get<unsigned int>("line_color"),Plot->second.get<double>("transparency"));
    }

    // OPTENTRY x and y title
    if (child_xtitle)
      histo_1D->GetXaxis()->SetTitle((Canv.get<std::string>("x_axisTitle")).c_str());

    if (child_ytitle)
      histo_1D->GetYaxis()->SetTitle((Canv.get<std::string>("y_axisTitle")).c_str());

    // OPTENTRY normalize
    bool normalize = false;
    boost::optional<bool> child_normalize = Plot->second.get_optional<bool>("normalize");
    
    if (child_normalize) // key exists
      normalize = child_normalize.get();
    
    if (normalize)
      histo_1D->Scale(1./histo_1D->Integral());
    
    histo_1D->Scale(Plot->second.get<double>("scaling"));
    
    histo_1D->SetLineColor(Plot->second.get<unsigned int>("line_color"));
    //histo_1D->SetLineWidth(2);

    histo_1D->GetXaxis()->SetTitleSize(0.06);
    histo_1D->GetYaxis()->SetTitleSize(0.06);
    histo_1D->GetXaxis()->SetLabelSize(0.06);
    histo_1D->GetYaxis()->SetLabelSize(0.06);

    histo_1D->GetXaxis()->SetNdivisions(506);
    histo_1D->GetYaxis()->SetNdivisions(508);

    histo_1D->GetXaxis()->SetTitleOffset(1.15);
    histo_1D->GetYaxis()->SetTitleOffset(1.15);
    histo_1D->GetXaxis()->SetLabelOffset(0.02);
    histo_1D->GetYaxis()->SetLabelOffset(0.01);

    // get maximum x value
    xMax = histo_1D->GetXaxis()->GetBinUpEdge(histo_1D->GetXaxis()->GetLast());

    if(yMax<1) histo_1D->GetYaxis()->SetTitleOffset(1.35);
    if(yMax>1 && yMax<10) histo_1D->GetYaxis()->SetTitleOffset(0.95);
    if(yMax>10 && yMax<100) histo_1D->GetYaxis()->SetTitleOffset(0.95);
    if(yMax>=100 && yMax<1000) histo_1D->GetYaxis()->SetTitleOffset(1.1);
    if(yMax>=1000 && yMax<10000) histo_1D->GetYaxis()->SetTitleOffset(1.25);
    if(yMax>=10000 && yMax<100000) histo_1D->GetYaxis()->SetTitleOffset(1.45);
    if(yMax>=10000 && yMax<1000000) histo_1D->GetYaxis()->SetTitleOffset(1.55);
    if(yMax>=1000000) histo_1D->GetYaxis()->SetTitleOffset(1.3);
    if(yLogScale) histo_1D->GetYaxis()->SetTitleOffset(1.1);

    canv->cd();

    // set y range to maximum of all plots, determined above
    histo_1D->GetYaxis()->SetRangeUser(yMin,yMax);

    histo_1D->Draw((Plot->second.get<std::string>("drawing_opt")).c_str());

    legEntries++;
    // roughly determine length of longest legend entry title
    // to set x position of legend
    if(drawLegend){
      leg->AddEntry(histo_1D,Plot->second.get<std::string>("legend").c_str());
      entryLength = Plot->second.get<std::string>("legend").length();
      for (unsigned int n = 0; n != Plot->second.get<std::string>("legend").length(); n++){
        if (Plot->second.get<std::string>("legend")[n]=='#') entryLength-=1;
        if (Plot->second.get<std::string>("legend")[n]=='_') entryLength-=4; // e.g. _{x}
        if (Plot->second.get<std::string>("legend")[n]=='^') entryLength-=4; // e.g. ^{x}
      }
      if(entryLength>maxEntryLength) maxEntryLength=entryLength;
    }

  }  //loop over plots

  // set legend position. if at default position (top right),
  // choose one of two x positions depending on maxentry length
  if(drawLegend){
    if(legendPos=="topleft"){
      leg->SetY2(0.96);
      leg->SetY1(leg->GetY2()-legEntries*0.05);
      leg->SetX1(0.125);
      leg->SetX2(leg->GetX1()+0.2);
    } else {
      leg->SetY2(0.96);
      leg->SetY1(leg->GetY2()-legEntries*0.05);
      if(maxEntryLength<10) leg->SetX1(0.72);
      else leg->SetX1(0.62);
      leg->SetX2(leg->GetX1()+0.2);
    }
    leg->Draw();
  }

  if(yMax<1) gPad->SetLeftMargin(0.15);
  if(yMax>1 && yMax<100) gPad->SetLeftMargin(0.11);
  if(yMax>=100 && yMax<1000) gPad->SetLeftMargin(0.13);
  if(yMax>=1000 && yMax<10000) gPad->SetLeftMargin(0.14); //11
  if(yMax>=10000 && yMax<1000000) gPad->SetLeftMargin(0.17);
  if(yMax>=1000000) gPad->SetLeftMargin(0.14);
  if(yLogScale) gPad->SetLeftMargin(0.13);

  gPad->SetRightMargin(0.05);
  if(xMax>=1000 && xMax<10000) gPad->SetRightMargin(0.06);
  if(xMax>=10000 && xMax<1000000) gPad->SetRightMargin(0.07);

  gPad->SetTopMargin(0.02);
  gPad->SetBottomMargin(0.15);

  gPad->RedrawAxis(); // axis possibly hidden due to fillstyle

  //---------------------//
  //  saving the canvas  //
  //---------------------//
  
  //I retrieve the list of formats to save
  std::string format_string = Canv.get<std::string>("formats");
  
  //now I loop over the output formats
  std::string format;
  std::string delimiter = ";";
  decltype(format_string.find(delimiter)) pos = 0;
  
  //loop over formats
  while(((pos = format_string.find(delimiter)) != std::string::npos)
        && (format_string.size() != 0)){
    format = format_string.substr(0, pos);
    
    //gPad->Modified();

    gPad->Update();
    canv->SaveAs((outPath + Canv.get<std::string>("output_name") + "." + format).c_str());
    
    format_string.erase(0, pos + delimiter.length());
  }  //loop over formats
  
  return;
}

//function to draw TH2D
void draw_TH2D(TFile *inFile, std::string outPath, const pt::ptree Canv){

  //defining the Canvas
  TCanvas *canv = new TCanvas("canv", "",
                              Canv.get<double>("width"),
                              Canv.get<double>("height"));
  gStyle->SetOptStat(0);
  
  //---------------------//
  //  drawing the plots  //
  //---------------------//
  
  TH2D *histo_2D  = new TH2D();
  
  boost::optional<std::string> child_xtitle = Canv.get_optional<std::string>("x_axisTitle");
  boost::optional<std::string> child_ytitle = Canv.get_optional<std::string>("y_axisTitle");
  
  //loop over plots
  for(pt::ptree::const_iterator Plot = Canv.get_child("Plots").begin();
      Plot != Canv.get_child("Plots").end(); ++Plot){
    
    histo_2D = (TH2D*) inFile->Get((Plot->second.get<std::string>("input_name")).c_str());

    if(histo_2D==nullptr){
      std::cerr << "ERROR: histogram " << Plot->second.get<std::string>("input_name") << " not found in file" << std::endl;
      throw;
    }
    
    if(histo_2D->GetEntries() == 0){
      std::cerr << "WARNING: empty histogram!" << Plot->second.get<std::string>("input_name") << std::endl;
    }

    canv->cd();
    
    histo_2D->SetTitle("");

    // OPTENTRY x and y title
    if (child_xtitle)
      histo_2D->GetXaxis()->SetTitle((Canv.get<std::string>("x_axisTitle")).c_str());
    
    if (child_ytitle)
      histo_2D->GetYaxis()->SetTitle((Canv.get<std::string>("y_axisTitle")).c_str());
    
    histo_2D->GetXaxis()->SetTitleSize(0.06);
    histo_2D->GetYaxis()->SetTitleSize(0.06);
    histo_2D->GetXaxis()->SetLabelSize(0.06);
    histo_2D->GetYaxis()->SetLabelSize(0.06);
    histo_2D->GetZaxis()->SetLabelSize(0.06);

    histo_2D->GetXaxis()->SetNdivisions(506);
    histo_2D->GetYaxis()->SetNdivisions(508);

    histo_2D->GetXaxis()->SetTitleOffset(1.15);
    histo_2D->GetYaxis()->SetTitleOffset(1.15);
    histo_2D->GetXaxis()->SetLabelOffset(0.02);
    histo_2D->GetYaxis()->SetLabelOffset(0.01);

    double yMax = histo_2D->GetYaxis()->GetXmax();
    if(yMax<1) histo_2D->GetYaxis()->SetTitleOffset(1.35);
    if(yMax>1 && yMax<10) histo_2D->GetYaxis()->SetTitleOffset(0.95);
    if(yMax>10 && yMax<100) histo_2D->GetYaxis()->SetTitleOffset(0.95);
    if(yMax>=100 && yMax<1000) histo_2D->GetYaxis()->SetTitleOffset(1.1);
    if(yMax>=1000 && yMax<10000) histo_2D->GetYaxis()->SetTitleOffset(1.25);
    if(yMax>=10000 && yMax<100000) histo_2D->GetYaxis()->SetTitleOffset(1.45);
    if(yMax>=10000 && yMax<1000000) histo_2D->GetYaxis()->SetTitleOffset(1.55);
    if(yMax>=1000000) histo_2D->GetYaxis()->SetTitleOffset(1.3);

    if(yMax<1) gPad->SetLeftMargin(0.16);
    if(yMax>1 && yMax<100) gPad->SetLeftMargin(0.12);
    if(yMax>=100 && yMax<1000) gPad->SetLeftMargin(0.14);
    if(yMax>=1000 && yMax<10000) gPad->SetLeftMargin(0.15); //11
    if(yMax>=10000 && yMax<1000000) gPad->SetLeftMargin(0.18);
    if(yMax>=1000000) gPad->SetLeftMargin(0.15);
    gPad->Update();

    histo_2D->Scale(Plot->second.get<double>("scaling"));
    histo_2D->Draw((Plot->second.get<std::string>("drawing_opt")).c_str());
    
  }  //loop over plots

  
  //-------------------//
  //  saving the plot  //
  //-------------------//
  
  //I retrieve the list of formats to save
  std::string format_string = Canv.get<std::string>("formats");
  
  //now I loop over the output formats

  std::string format;
  std::string delimiter = ";";
  
  decltype(format_string.find(delimiter)) pos = 0;
  
  //loop over formats
  while(((pos = format_string.find(delimiter)) != std::string::npos)
        && (format_string.size() != 0)){
    format = format_string.substr(0, pos);
    
    //gPad->Modified();
    gPad->SetTopMargin(0.04);
    gPad->SetBottomMargin(0.15);
    gPad->SetRightMargin(0.12);
    gPad->Update();
    canv->SaveAs((outPath + Canv.get<std::string>("output_name") + "." + format).c_str());
    
    format_string.erase(0, pos + delimiter.length());
  }  //loop over formats
  
  return;
}

void draw_stuff(TFile* inFile, const std::string& outPath, const pt::ptree& plotconf){

  lhcbStyle();

  TCanvas canv("c1","myCanvas",plotconf.get("CanvXSize",2048),plotconf.get("CanvYSize",1280));
  gPad->SetLeftMargin  (plotconf.get("lmargin",0.12));
  gPad->SetBottomMargin(plotconf.get("bmargin",0.12));
  gPad->SetRightMargin (plotconf.get("rmargin",0.01));
  gPad->SetTopMargin   (plotconf.get("tmargin",0.01));
  if(plotconf.get_optional<bool>("logx"))gPad->SetLogx();
  if(plotconf.get_optional<bool>("logy"))gPad->SetLogy();
  if(plotconf.get_optional<bool>("logz"))gPad->SetLogz();
  if(plotconf.get_optional<int>("Palette"))gStyle->SetPalette(plotconf.get<int>("Palette"));

  //We need to push the legends and pavetexts into a vector, so they are not out of scope when saving the canvas
  vector< unique_ptr<TLegend> > legs;
  vector< unique_ptr<TPaveText> > pts;
  vector< unique_ptr<TLatex> > ltxs;

  //individual plots. different types can be mixed
  for(const auto& plot : plotconf.get_child("Plots")){

    std::printf("\033[0;36m%-20.20s \033[1;37m%-8.8s\033[0m Requested to add %s to canvas\n","drawPlots:draw_stuff","INFO:",plot.second.get<std::string>("name").data());

    //find the plottable object in file. problem: it's of type TObject*
    //i tried a bit, but there is no way to declare types at runtime, such that you would be able to cast back to TH1*, TGraph* or TMultiGraph*
    //maybe boost::variant can do it, but i didn't see the way there
    //instead, let's do a dirty trick and call a templated lambda
    auto obj = inFile->FindObjectAny(plot.second.get<std::string>("name").data());

    //lambdas to configure axes. TGraph was making problems with the Zaxis, so this had to be split in 1 and 2D...
    //TMuligraph can't do much, so this requires another split
    //the following lambda is common to both versions
    auto rescale_axis = [&plotconf] (TAxis* axis, const pt::ptree& ax, auto plot_obj) {
      //parse minimum and maximum from plot
      if( (ax.get_optional<double>("min") && ax.get_optional<double>("max")) //rescale in user defined way or parse range,
          || ax.get_optional<bool>("parse_range") ){  //in this way it can still be user defined, but also scaled with minsub and maxadd
        auto min = 0.f, max = 1.f;
        //logscale plot. this only works for y and z axis (1 or 2D) and assumes nothing too freaky, i.e. 1 D plots with logz or 2D plots with logy and linear z
        if(plotconf.get_optional<bool>("logy") || plotconf.get_optional<bool>("logz")){
          max = pow(10,log10(plot_obj->GetMaximum())+ax.get("maxadd",0.1)*(log10(plot_obj->GetMaximum())-log10(plot_obj->GetMinimum())));
          min = pow(10,log10(plot_obj->GetMinimum())-ax.get("minsub",0.1)*(log10(plot_obj->GetMaximum())-log10(plot_obj->GetMinimum())));
        }
        else {//linear plot
          min = plot_obj->GetMinimum() - ax.get("minsub",0.0)*(plot_obj->GetMaximum() - plot_obj->GetMinimum());
          max = plot_obj->GetMaximum() + ax.get("maxadd",0.1)*(plot_obj->GetMaximum() - plot_obj->GetMinimum());
        }
        axis->SetRangeUser(ax.get("min",min),ax.get("max",max));
      }
    };

    auto config_single_axis = [] (TAxis*& axis, const pt::ptree& ax) {
      axis->SetLabelOffset(ax.get("LabelOffset",axis->GetLabelOffset()));
      axis->SetLabelSize  (ax.get("LabelSize",axis->GetLabelSize()));
      axis->SetNdivisions (ax.get("Ndivisions",axis->GetNdivisions()));
      axis->SetTickLength (ax.get("TickLength",axis->GetTickLength()));
      axis->SetTickSize   (ax.get("TickSize",0.03));
      axis->SetTitle      (ax.get("Title",static_cast<std::string>(axis->GetTitle())).data());
      axis->SetTitleOffset(ax.get("TitleOffset",axis->GetTitleOffset()));
      axis->SetTitleSize  (ax.get("TitleSize",axis->GetTitleSize()));
    };

    auto conf_mgaxes = [&plot,&config_single_axis] (auto plot_obj) {
      if(plot.second.get_child_optional("axes")){
        for(const auto& ax : plot.second.get_child("axes")) {
          TAxis* axis;
          if      (ax.first == "X")axis = plot_obj->GetXaxis();
          else if (ax.first == "Y")axis = plot_obj->GetYaxis();
          else std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m There is no %s axis in %s\n","drawPlots:draw_stuff","WARNING:",ax.first.data(),plot_obj->GetName());
          config_single_axis(axis,ax.second);
          axis->SetRangeUser(ax.second.get("min",0.f),ax.second.get("max",1.f));
        }
      }
    };

    auto conf_2Daxes = [&plot,&config_single_axis,&rescale_axis] (auto plot_obj) {
      if(plot.second.get_child_optional("axes")){
        for(const auto& ax : plot.second.get_child("axes")) {
          TAxis* axis;
          if      (ax.first == "X")axis = plot_obj->GetXaxis();
          else if (ax.first == "Y")axis = plot_obj->GetYaxis();
          else std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m There is no %s axis in %s\n","drawPlots:draw_stuff","WARNING:",ax.first.data(),plot_obj->GetName());
          config_single_axis(axis,ax.second);
          rescale_axis(axis,ax.second,plot_obj);
        }
      }
    };

    auto conf_axes = [&plot,&config_single_axis,&rescale_axis] (auto plot_obj) {
      if(plot.second.get_child_optional("axes")){
        for(const auto& ax : plot.second.get_child("axes")) {
          TAxis* axis;
          if      (ax.first == "X")axis = plot_obj->GetXaxis();
          else if (ax.first == "Y")axis = plot_obj->GetYaxis();
          else if (ax.first == "Z")axis = plot_obj->GetZaxis();
          else std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m There is no %s axis in %s\n","drawPlots:draw_stuff","WARNING:",ax.first.data(),plot_obj->GetName());
          config_single_axis(axis,ax.second);
          rescale_axis(axis,ax.second,plot_obj);
          if(ax.second.get_child_optional("PaletteAxis")){
            gPad->Update();
            auto pal = static_cast<TPaletteAxis*>(plot_obj->FindObject("palette"));
            if(pal == nullptr){
              std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m Could not find TPaletteAxis \" palette \" in %s\n","drawPlots:draw_stuff","WARNING:",plot_obj->GetName());
              return;
            }
            if(ax.second.get_optional<std::string>("PaletteAxis.LabelColor"))
              pal->SetLabelColor(LHCb::color(ax.second.get<std::string>("PaletteAxis.LabelColor")));
            if(ax.second.get_optional<int>("PaletteAxis.LabelFont"))
              pal->SetLabelFont(ax.second.get<int>("PaletteAxis.LabelFont"));
            if(ax.second.get_optional<float>("PaletteAxis.LabelOffset"))
              pal->SetLabelOffset(ax.second.get<float>("PaletteAxis.LabelOffset"));
            if(ax.second.get_optional<double>("PaletteAxis.margin"))
              pal->SetX1NDC(1 - gPad->GetRightMargin() + ax.second.get<double>("PaletteAxis.margin"));
            if(ax.second.get_optional<double>("PaletteAxis.width"))
              pal->SetX2NDC(pal->GetX1NDC() + ax.second.get<double>("PaletteAxis.width"));
            pal->SetY1NDC(ax.second.get("PaletteAxis.Y1NDC",pal->GetY1NDC()));
            pal->SetY2NDC(ax.second.get("PaletteAxis.Y2NDC",pal->GetY2NDC()));
          }
        }
      }
    };

    //lambda to configure basic TAttLine, TAttMarker, TAttFill attributes
    auto conf_attributes = [&plot] (auto plot_obj) {
      if(plot.second.get_optional<std::string>("FillColor"))
        plot_obj->SetFillColorAlpha(LHCb::color(plot.second.get<std::string>("FillColor")),plot.second.get("FillColorAlpha",1.f));
      else
        plot_obj->SetFillColorAlpha(plot_obj->GetFillColor(),plot.second.get("FillColorAlpha",1.f));
      plot_obj->SetFillStyle(plot.second.get("FillStyle",plot_obj->GetFillStyle()));
      if(plot.second.get_optional<std::string>("LineColor"))
        plot_obj->SetLineColorAlpha(LHCb::color(plot.second.get<std::string>("LineColor")),plot.second.get("LineColorAlpha",1.f));
      else
        plot_obj->SetLineColorAlpha(plot_obj->GetLineColor(),plot.second.get("LineColorAlpha",1.f));
      plot_obj->SetLineStyle(plot.second.get("LineStyle",plot_obj->GetLineStyle()));
      plot_obj->SetLineWidth(plot.second.get("LineWidth",plot_obj->GetLineWidth()));
      if(plot.second.get_optional<std::string>("MarkerColor"))
        plot_obj->SetMarkerColorAlpha(LHCb::color(plot.second.get<std::string>("MarkerColor")),plot.second.get("MarkerColorAlpha",1.f));
      else
        plot_obj->SetMarkerColorAlpha(plot_obj->GetMarkerColor(),plot.second.get("MarkerColorAlpha",1.f));
      plot_obj->SetMarkerStyle(plot.second.get("MarkerStyle",plot_obj->GetMarkerStyle()));
      plot_obj->SetMarkerSize(plot.second.get("MarkerSize",plot_obj->GetMarkerSize()));
      plot_obj->SetTitle(plot.second.get("Title",plot_obj->GetTitle()).data());
    };

    auto hide_exp = [&pts] (bool hide_x) {
      std::unique_ptr<TPaveText> HideExp(new TPaveText);
      if(hide_x){
        HideExp->SetX1NDC(1.002-gPad->GetRightMargin());
        HideExp->SetY1NDC(gPad->GetBottomMargin());
        HideExp->SetX2NDC(1);
        HideExp->SetY2NDC(gPad->GetBottomMargin()+0.05);
      }
      else{
        HideExp->SetX1NDC(gPad->GetLeftMargin());
        HideExp->SetY1NDC(1.002-gPad->GetTopMargin());
        HideExp->SetX2NDC(gPad->GetLeftMargin()+0.05);
        HideExp->SetY2NDC(1);
      }
      HideExp->SetBorderSize(0);
      HideExp->SetFillColor(kWhite);
      HideExp->SetTextColor(kWhite);
      HideExp->SetFillStyle(1001);
      HideExp->AddText(" ");
      pts.push_back(std::move(HideExp));
    };

    //cast back to (plot-wise configurable base class of) original type and pass it to the lambdas above
    if (obj == nullptr){
      std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m Object %s not found in %s\n","drawPlots:draw_stuff","WARNING:",plot.second.get<std::string>("name").data(),inFile->GetName());
      continue;
    }
    else if(obj->InheritsFrom("TH1")){
      auto hist = static_cast<TH1*>(obj);
      conf_axes(hist);
      conf_attributes(hist);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      hist->Draw(plot.second.get("drawing_opt","e1p").data());
    }
    else if(obj->InheritsFrom("TF1")){
      auto func = static_cast<TF1*>(obj);
      conf_axes(func);
      conf_attributes(func);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      func->Draw(plot.second.get("drawing_opt","c").data());
    }
    else if(obj->InheritsFrom("TGraph2D")){
      auto tg2 = static_cast<TGraph2D*>(obj);
      conf_axes(tg2);
      conf_attributes(tg2);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      tg2->Draw(plot.second.get("drawing_opt","cont4z").data());
    }
    else if(obj->InheritsFrom("TGraph")){
      auto gr = static_cast<TGraph*>(obj);
      conf_2Daxes(gr);
      conf_attributes(gr);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      gr->Draw(plot.second.get("drawing_opt","ac").data());
    }
    else if(obj->InheritsFrom("TEfficiency")){
      std::printf("\033[0;36m%-20.20s \033[1;37m%-8.8s\033[0m TEfficiency does not allow to manipulate axes. If this is desired, please make a TH1 derived object from it\n","drawPlots:draw_stuff","INFO:");
      auto eff = static_cast<TEfficiency*>(obj);
      conf_attributes(eff);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      eff->Draw(plot.second.get("drawing_opt","ap").data());
    }
    else if(obj->InheritsFrom("TMultiGraph")){
      std::printf("\033[0;36m%-20.20s \033[1;37m%-8.8s\033[0m You are using TMultiGraph. This only allows to configure the canvas, part of the axes and adding labels\n","drawPlots:draw_stuff","INFO:");
      auto mg = static_cast<TMultiGraph*>(obj);
      conf_mgaxes(mg);
      if(plotconf.get_optional<bool>("HideExpX"))hide_exp(true);
      if(plotconf.get_optional<bool>("HideExpY"))hide_exp(false);
      mg->SetTitle(plot.second.get("title",static_cast<std::string>(mg->GetTitle())).data());
      mg->Draw(plot.second.get("drawing_opt","ac").data());
    }
    else {
      std::printf("\033[0;36m%-20.20s \033[1;33m%-8.8s\033[0m Object %s does not inherit from TH1, TF1, TGraph, TGraph2D, TMultiGraph or TEfficiency\n "
                  "%29.29s Please check if the requested class can be plotted and - if yes - add it to source file\n","drawPlots:draw_stuff","WARNING:",plot.second.get<std::string>("name").data()," ");
      continue;
    }

  }//end of individual plots loop

  if(plotconf.get_child_optional("legends")){
    for(const auto& l : plotconf.get_child("legends")) {
      std::unique_ptr<TLegend> leg(new TLegend);
      leg->SetHeader(l.second.get("header",static_cast<std::string>("")).data());
      leg->SetX1NDC(l.second.get<double>("xlo"));
      leg->SetY1NDC(1-gPad->GetTopMargin()-l.second.get<double>("dtop")-(l.second.get<double>("height")*l.second.get<double>("TextSize",0.05)));
      leg->SetX2NDC(1-gPad->GetRightMargin()-l.second.get<double>("dright"));
      leg->SetY2NDC(1-gPad->GetTopMargin()-l.second.get<double>("dtop"));
      leg->SetBorderSize(0);
      leg->SetFillColor(kWhite);
      leg->SetTextAlign(12);
      leg->SetFillStyle(0);
      leg->SetTextSize(l.second.get<double>("TextSize",0.05));
      for(const auto& v : l.second )
        if( v.second.get_optional<std::string>("name") )
          leg->AddEntry(canv.FindObject(v.second.get<std::string>("name").data()), v.second.get<std::string>("label").data(), v.second.get<std::string>("option","lpf").data());
      legs.push_back(std::move(leg));
    }
  }
  for(const auto& leg : legs)
    leg->Draw();

  if(plotconf.get_child_optional("pavetexts")){
    for(const auto& p : plotconf.get_child("pavetexts")) {
      std::unique_ptr<TPaveText> pt(new TPaveText);
      pt->SetX1NDC(p.second.get<double>("xlo"));
      pt->SetY1NDC(1-gPad->GetTopMargin()-p.second.get<double>("dtop")-(p.second.get<double>("height")*p.second.get<double>("TextSize",0.05)));
      pt->SetX2NDC(1-gPad->GetRightMargin()-p.second.get<double>("dright"));
      pt->SetY2NDC(1-gPad->GetTopMargin()-p.second.get<double>("dtop"));
      pt->SetBorderSize(0);
      pt->SetFillColor(kWhite);
      pt->SetTextAlign(12);
      pt->SetFillStyle(0);
      pt->SetTextSize(p.second.get<double>("TextSize",0.05));
      if(p.second.get_child_optional("AddText"))
        for(const auto& v : p.second.get_child("AddText") )
          pt->AddText(v.first.data());
      pts.push_back(std::move(pt));
    }
  }
  for(const auto& pt : pts)
    pt->Draw();

  if(plotconf.get_child_optional("latexts")){
    for(const auto& lx : plotconf.get_child("latexts")) {
      std::unique_ptr<TLatex> ltx(new TLatex);
      ltx->SetNDC();
      ltx->SetText(lx.second.get<double>("X"),lx.second.get<double>("Y"),lx.second.get<std::string>("Text").data());
      ltx->SetTextAlign(lx.second.get<int>("TextAlign",11));
      ltx->SetTextAngle(lx.second.get<float>("TextAngle",0.f));
      ltx->SetTextColor(LHCb::color(lx.second.get("TextColor","black")));
      if(lx.second.get_optional<int>("TextFont"))
        ltx->SetTextFont(lx.second.get<int>("TextFont"));
      if(lx.second.get_optional<float>("TextSize"))
        ltx->SetTextSize(lx.second.get<float>("TextSize"));
      ltxs.push_back(std::move(ltx));
    }
  }
  for(const auto& ltx : ltxs){
    ltx->Draw();
  }


  for(const auto& ex : plotconf.get_child("outputformats"))
    canv.SaveAs((outPath + plotconf.get<std::string>("output_name") + "." + ex.first).data());
}


int main(int argc, char** argv){

  //gROOT->ProcessLine(".L lhcbstyle.C");
  //lhcbStyle();

  //-----------------------//
  //  parsing job options  //
  //-----------------------//

  std::string configFileName = "";
  std::string inPath = "";
  std::string outPath = "";

  extern char* optarg;

  int ca;

  //parsing the input options
  while ((ca = getopt(argc, argv, "c:i:o:h")) != -1){
    switch (ca){

      case 'c':
        configFileName = optarg;
        break;

      case 'i':
        inPath = optarg;
        break;

      case 'o':
        outPath = optarg;
        break;

      case 'h':
        std::cout << "-- Help --" << std::endl;
        std::cout << "-c : configuration file name." << std::endl;
        std::cout << "-i : input path." << std::endl;
        std::cout << "-o : output path." << std::endl;
        std::cout << std::endl;
        exit(EXIT_SUCCESS);
        break;

      default :
        std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
        exit(EXIT_FAILURE);

    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o")) != -1)

  //printing of acquired options
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "inPath = " << inPath << std::endl;
  std::cout << "outPath = " << outPath << std::endl;

  //check of the acquired options
  if((inPath == "") || (outPath == "") || (configFileName == "")){
    std::cout << "Error: config file name or input/output paths not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }


  //----------------------------------//
  //  reading the configuration file  //
  //----------------------------------//

  //create empty property tree object
  pt::ptree configtree;

  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);

  //--------------------------//
  //  loop over data to plot  //
  //--------------------------//

  //loop over input files
  for(pt::ptree::const_iterator File = configtree.get_child("Files").begin();
      File != configtree.get_child("Files").end(); ++File){

    //reading input file
    TFile* inFile = TFile::Open((inPath + File->second.get<std::string>("file_name")).c_str(),"READ");

    if(inFile == nullptr){
      std::cout << "Error: input file does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }

    //loop over plots to draw
    for(pt::ptree::const_iterator Canv = File->second.get_child("Canvas").begin();
        Canv != File->second.get_child("Canvas").end(); ++Canv){

      //now I have to select TH1D or TH2D
      if(Canv->second.get_optional<std::string>("histo")){
        if(Canv->second.get<std::string>("histo") == "TH1D")
          draw_TH1D(inFile, outPath, Canv->second);

        else if(Canv->second.get<std::string>("histo") == "TH2D")
          draw_TH2D(inFile, outPath, Canv->second);
      }
      else draw_stuff(inFile, outPath, Canv->second);

    } //loop over plots to draw

    //closing file
    inFile->Close();

    //memory cleaning
    delete inFile;

  }  //loop over input files

  return 0;

}
