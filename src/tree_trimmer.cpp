/*!
  @brief: a simple TTree::CopyTree() would do the trick, but that doesn't work with friendtrees
**/

//C++
#include <type_traits>//decltype
#include <vector>
#include <exception>
#include <string>
//root
#include <TObject.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TLeaf.h>
#include <TBranch.h>
#include <TFriendElement.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEntryList.h>
//local
#include <../IOjuggler/IOjuggler.h>
#include "../cutter.h"

#ifdef NTUPLE_GIZMO_GIT_HASH
  #define NTUPLE_GIZMO_HASH NTUPLE_GIZMO_GIT_HASH
#else
  #define NTUPLE_GIZMO_HASH " "
#endif

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "c:d:i:o:t:v:h","nonoptions: any number of <file:friendtree> combinations");
  MessageService msg_svc("tree_trimmer",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msg_svc.infomsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_HASH));
  auto configtree = get_ptree(options.get<std::string>("config"));
  auto wd = options.get<std::string>("workdir");
  auto input = get_file(options.get<std::string>("infilename"),wd);
  auto tree = get_obj<TTree>(input,options.get<std::string>("treename"));

  if(!configtree.get_child_optional("variables")) throw std::runtime_error("mandatory child \"variables\" not given in config file");
  const bool noImplicitCuts=(configtree.get_child_optional("noImplicitCuts"));
  

  msg_svc.infomsg("trimming input tree(s) with cut " + configtree.get<std::string>("basiccuts","") + " and writing out " + configtree.get_child("variables").size() + " variables");

  if(noImplicitCuts) msg_svc.infomsg("Implicit cuts switched off");
  

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  if(options.get_child_optional("extraargs")){
    for(const auto& ea : options.get_child("extraargs")){
      auto ff = get_file(static_cast<std::string>(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(0)))->String().Data()),options.get<std::string>("workdir"));
      fes.push_back(tree->AddFriend(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(1)))->String().Data(),ff));
    }
  }

  //apply cuts and save the indices of entries passing them
  tree->Draw(">>elist",configtree.get<std::string>("basiccuts","").data(),"entrylist");
  //get and loop this list
  TEntryList* list = static_cast<TEntryList*>(gDirectory->Get("elist"));
  tree->SetEntryList(list);

  tree->SetBranchStatus("*",0);

  //initialize leaves and outputs for filling the output tree
  const unsigned int nvars = static_cast<unsigned int>(configtree.get_child("variables").size());
  std::vector<TLeaf*> var_leaves(nvars,nullptr);
  std::vector<double> output_values(nvars,0.0);

  //create output file
  TFile of((wd + "/" + options.get<std::string>("outfilename")).data(),"recreate");
  std::string outtreename = configtree.get("outtreename",options.get<std::string>("treename"));
  TTree otree(outtreename.data(),outtreename.data());

  //initialise implicit cuts
  cutset implicit_cuts;
  //loop variables, do the branch logistics and the implicit cuts
  unsigned int var_counter = 0u;
  //save computing time by calling different loop if variables need to be transformed
  bool do_variable_transformation = false;
  std::vector<std::string> var_names(nvars);
  std::vector<std::string> new_var_names(nvars);
  for(const auto& var : configtree.get_child("variables")){
    var_names[var_counter] = var.first;
    msg_svc.debugmsg("adding variable: " + var_names[var_counter]);
    tree->SetBranchStatus(var_names[var_counter].data(),1);
    var_leaves[var_counter] = tree->GetLeaf(var_names[var_counter].data());
    new_var_names[var_counter] = var.second.get("new_name",var_names[var_counter]);
    otree.Branch(new_var_names[var_counter].data(),&output_values[var_counter],(new_var_names[var_counter]+"/D").data());
    //check if new variables need to be parsed
    if(!do_variable_transformation && new_var_names[var_counter] != var_names[var_counter] &&
       (new_var_names[var_counter].find("log_") != std::string::npos || new_var_names[var_counter].find("sqrt_") != std::string::npos ||
        new_var_names[var_counter].find("atan_") != std::string::npos || new_var_names[var_counter].find("NNtfd_") != std::string::npos ))
      do_variable_transformation = true;
    //do implicit cuts
    if(!(var.second.get_optional<double>("low") || var.second.get_optional<double>("high"))) throw std::runtime_error("mandatory range of variable not given");
    implicit_cuts.addCut(new range((var_names[var_counter] + "Range").data(),var_names[var_counter].data(),&output_values[var_counter],var.second.get<double>("low"),var.second.get<double>("high")));
    var_counter++;
  }

  //loop entry list and save the new tree
  auto nentries = list->GetN();
  if(do_variable_transformation){
    for (decltype(nentries) evt = 0; evt < nentries; evt++){
      //important: list->GetEntry(evt) returns the index of the passed event in the original tree
      tree->GetEntry(list->GetEntry(evt));
      //fill the output tree
      for(typename std::decay<decltype(nvars)>::type ix = 0; ix < nvars; ix++)
        if(var_names[ix] != new_var_names[ix]){
          if(new_var_names[ix].find("log_") != std::string::npos)
            output_values[ix] = std::log10(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("sqrt_") != std::string::npos)
            output_values[ix] = std::sqrt(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("atan_") != std::string::npos)
            output_values[ix] = std::atan(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("NNtfd_") != std::string::npos)
            output_values[ix] = std::log10(static_cast<double>(var_leaves[ix]->GetValue(0))/(1-static_cast<double>(var_leaves[ix]->GetValue(0))));
          else
            output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
        }
        else
          output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
      if(noImplicitCuts || implicit_cuts.passall())
        otree.Fill();
    }
  }
  else{
    //loop without transformation
    for (decltype(nentries) evt = 0; evt < nentries; evt++){
      //important: list->GetEntry(evt) returns the index of the passed event in the original tree
      tree->GetEntry(list->GetEntry(evt));
      //fill the output tree
      for(typename std::decay<decltype(nvars)>::type ix = 0; ix < nvars; ix++)
        output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
      if(noImplicitCuts || implicit_cuts.passall())
        otree.Fill();
    }
  }
  otree.Write();
  of.Close();
  input->Close();
  return 0;
}
