//C++
#include <type_traits>//decltype
#include <vector>
#include <exception>
#include <string>
//root
#include <TObject.h>
#include <TFile.h>
#include <TChain.h>
#include <TString.h>
//local
#include <../IOjuggler/IOjuggler.h>

// very simple tool to check if an object can be retrieved from a root file
// will throw if that does not work

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "d:i:t:v:h");
  MessageService msg_svc("checkRootFile",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  auto wd = options.get<std::string>("workdir");
  auto fn = options.get<std::string>("infilename");
  auto tn = options.get<std::string>("treename");

  auto input = get_file(fn,wd);
  auto obj = get_obj<TObject>(input,tn);
  if(obj==nullptr) return 1; // get_obj should have thrown by now if this is the case
  // but we have to use obj for something to pass the compiler warnings 
  return 0;
}
