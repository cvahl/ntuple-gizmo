//C++
#include <algorithm>
#include <map>
#include <string>
#include <type_traits>//decltype
#include <vector>
//root
#include <TFile.h>
#include <TH2D.h>
#include <TLeaf.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>
#include <TTree.h>
#include <boost/lexical_cast.hpp>
//local
#include "../IOjuggler/IOjuggler.h"
#include "../IOjuggler/debug_helpers.h"

//script that parses misID variables created from include/misID_beta.h and makes histograms

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "d:i:o:t:v:h");
  MessageService msg_svc("make_misID_hists",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  auto wd = options.get<std::string>("workdir");
  auto fn = options.get<std::string>("infilename");
  auto tn = options.get<std::string>("treename");
  auto on = options.get<std::string>("outfilename");
  //parse nonoptions: number of bins and a bool if the histo-range should be parsed
  std::vector<int> peas;
  bool parse_hist_range = true;
  if(auto eas = options.get_child_optional("extraargs"))
    for(const auto& ea : *eas)
      peas.push_back(std::stoi(ea.second.data()));
  //if one of the arguments is 0, set the flag to false
  if(std::remove(peas.begin(),peas.end(),0) != peas.end())
    parse_hist_range = false;
  //remove values <= 1 from nonoptions, the rest should now define the number of bins in x and y
  peas.erase(std::remove_if(peas.begin(),peas.end(),[](auto& val){return val <= 1;}),peas.end());
  //set nbins
  int nbinsx = 100, nbinsy = 100;
  if(peas.size() == 1){nbinsx = peas[0];nbinsy = peas[0];}
  else if(peas.size() == 2){nbinsx = peas[0];nbinsy = peas[1];}

  msg_svc.infomsg("Requested to make histograms for all momentum asymmetry-mass combinations with "
                  + std::to_string(nbinsx) + "x" + std::to_string(nbinsy) + " bins");
  auto input = get_file(fn,wd);
  auto tree = get_obj<TTree>(input,tn);
  //switch off branches we don't need
  tree->SetBranchStatus("*",false);
  //get names of branches and push them in a vector
  const auto brar = tree->GetListOfBranches();
  std::vector<TString> bns;
  for(decltype(brar->GetEntries()) i = 0; i < brar->GetEntries(); i++)
    bns.emplace_back(brar->At(i)->GetName());
  //declare maps to fill the histos later
  std::map<std::string,TLeaf*> lvs;
  std::map<std::string,TH1D> rghs;
  std::map<std::pair<std::string,std::string>,TH2D> hists;
  //iterate interesting branches
  for(auto& bn : bns){
    if(bn.Tokenize("_")->GetEntries() != 2)continue;    
    auto get_string = [&bn] (auto&& pos) {
      return static_cast<std::string>(static_cast<TObjString*>(bn.Tokenize("_")->At(pos))->String().Data());
    };
    auto cmb = get_string(1);
    if(get_string(0) == "M" && std::all_of(cmb.begin(), cmb.end(),::isdigit)){
      msg_svc.debugmsg(bn);
      //switch branch on again, get M and trivial beta
      const auto mname = "M_"+cmb;
      tree->SetBranchStatus(mname.data(),true);
      lvs[mname] = tree->GetLeaf(mname.data());
      const auto betaname = "beta_"+cmb;
      tree->SetBranchStatus(betaname.data(),true);
      lvs[betaname] = tree->GetLeaf(betaname.data());
      hists[std::make_pair(mname,betaname)] =
          std::move(TH2D((mname+"_"+betaname).data(),(";#beta_{"+cmb+"};M_{"+cmb+"} (MeV)").data(),
                         nbinsx,-1,-1,nbinsy,-1,-1));
      if(parse_hist_range){
        rghs[mname] = std::move(TH1D(mname.data(),"",10,-1,-1));
        rghs[betaname] = std::move(TH1D(betaname.data(),"",10,-1,-1));
      }
      //get beta permutations
      if(cmb.size() > 2){
        msg_svc.debugmsg(cmb);
        std::vector<unsigned int> cis;
        for(auto& bla : cmb)
          cis.push_back(boost::lexical_cast<unsigned int>(bla));
        for(decltype(cis.size()) i = 0; i < cis.size()-1; i++){
          std::rotate(cis.begin(),cis.begin()+1,cis.end());
          std::string permbetaname = "beta_";
          for(const auto& idx : cis)
            permbetaname += std::to_string(idx);
          msg_svc.debugmsg(permbetaname);
          tree->SetBranchStatus(permbetaname.data(),true);
          lvs[permbetaname] = tree->GetLeaf(permbetaname.data());
          hists[std::make_pair(mname,permbetaname)] =
              std::move(TH2D((mname+"_"+permbetaname).data(),
                             (";#beta_{"+permbetaname.substr(5)+"};M_{"+cmb+"} (MeV)").data(),
                             nbinsx,-1,-1,nbinsy,-1,-1));
          if(parse_hist_range)
            rghs[permbetaname] = std::move(TH1D(permbetaname.data(),"",10,-1,-1));//quantiles don't depend on binning
        }
      }
    }
  }
  msg_svc.infomsg("Initialized " + std::to_string(hists.size()) + " histograms");
  //loop tree for ranges
  const auto nentries = tree->GetEntries();
  const double one_percent = std::floor(nentries/100);
  unsigned int percentage  = static_cast<unsigned int>(one_percent);

  if(parse_hist_range){
    msg_svc.infomsg("Parsing ranges/removing outliers for all histograms");
    for(typename std::decay<decltype(nentries)>::type ev = 0; ev < nentries; ev++){
      tree->GetEntry(ev);
      if(ev == percentage){
        percentage += one_percent;
        loadbar(static_cast<unsigned int>(ev),static_cast<unsigned int>(nentries));
      }
      for(const auto& x : lvs)
        rghs[x.first].Fill(x.second->GetValue(0));
    }
    std::cout << "\n";
    percentage  = static_cast<unsigned int>(one_percent);//reset counter
    //get quantiles
    const int nqt = 2;
    double level[nqt] = {0.001,0.999};
    double quantiles[nqt];
    std::map<std::string,std::pair<double,double>> rgs;
    for(auto& x : rghs){
      msg_svc.debugmsg("Nentries 1D " + std::to_string(x.second.GetEntries()));
      x.second.GetQuantiles(nqt,quantiles,level);
      msg_svc.debugmsg("Quantiles " + std::to_string(quantiles[0]) + " " + std::to_string(quantiles[1]));
      //round to 10 MeV or 0.1 asymmetry
      msg_svc.debugmsg(x.first + " starts with M? " + boost::lexical_cast<std::string>(x.first.compare(0,1,std::string("M")) == 0));
      auto lo = x.first.compare(0,1,std::string("M")) == 0 ? std::floor(0.1*quantiles[0])*10 : std::floor(10*quantiles[0])*0.1;
      auto hi = x.first.compare(0,1,std::string("M")) == 0 ? std::ceil(0.1*quantiles[1])*10 : std::ceil(10*quantiles[1])*0.1;
      rgs[x.first] = std::make_pair(lo,hi);
    }
    for(auto& h2 : hists)
      h2.second.SetBins(nbinsx,rgs[h2.first.second].first,rgs[h2.first.second].second,
          nbinsy,rgs[h2.first.first].first,rgs[h2.first.first].second);
  }
  //fill the hists
  TFile of((wd+"/"+on).data(),"RECREATE");
  msg_svc.infomsg("Filling histograms");
  for(typename std::decay<decltype(nentries)>::type ev = 0; ev < nentries; ev++){
    tree->GetEntry(ev);
    if(ev == percentage){
      percentage += one_percent;
      loadbar(static_cast<unsigned int>(ev),static_cast<unsigned int>(nentries));
    }
    for(auto& h2 : hists)
      h2.second.Fill(lvs[h2.first.second]->GetValue(0),lvs[h2.first.first]->GetValue(0));
  }
  std::cout << "\n";
  of.cd();
  for(auto& h2 : hists)
    h2.second.Write();
  of.Close();
  return 0;
}
