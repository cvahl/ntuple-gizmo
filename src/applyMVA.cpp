/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  Evaluate TMVA weight files using boost property trees
*/
//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TString.h"
#include "TFriendElement.h"
//TMVA
#include "TMVA/Reader.h"
//STL
#include <iostream>
#include <vector>
#include <cmath>
//BOOST
#include <boost/property_tree/ptree.hpp>
//local
#include <../IOjuggler/debug_helpers.h>
#include <../IOjuggler/IOjuggler.h>

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "c:d:i:o:t:hm:v:","nonoptions: any number of <file:friendtree> combinations");
  MessageService msg_svc("applyMVA",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  auto input = get_file(options.get<std::string>("infilename"),options.get<std::string>("workdir"));
  auto tree = get_obj<TTree>(input,options.get<std::string>("treename"));
  auto configtree = get_ptree(options.get<std::string>("config"));  

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  if(options.get_child_optional("extraargs")){
    for(const auto& ea : options.get_child("extraargs")){
      auto ff = get_file(static_cast<std::string>(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(0)))->String().Data()),options.get<std::string>("workdir"));
      fes.push_back(tree->AddFriend(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(1)))->String().Data(),ff));
    }
  }

  tree->SetBranchStatus("*",0);
  //the first part of the weightfile name is the factory name, which was given as nodename in the trainMVA config file. Use that below
  auto factory_name = static_cast<std::string>(static_cast<TObjString *>(static_cast<TString>(options.get<std::string>("weightfile")).Tokenize("_")->At(0))->String());
  const unsigned int nvars = static_cast<unsigned int>(configtree.get_child("tasks." + factory_name + ".variables").size());
  unsigned int nspec = 0;
  if(configtree.get_child_optional("tasks." + factory_name + ".spectators"))
     nspec=static_cast<unsigned int>(configtree.get_child("tasks." + factory_name + ".spectators").size());

  std::vector<TLeaf*> var_leaves(nvars+nspec,nullptr);
  //prepare MVA reader
  TMVA::Reader* reader = new TMVA::Reader();
  std::vector<float> MVAvarvals(nvars+nspec,0.f);

  unsigned int var_counter = 0u;
  for(const auto& var : configtree.get_child("tasks." + factory_name + ".variables")){
    tree->SetBranchStatus(var.first.data(),1);
    var_leaves[var_counter] = tree->GetLeaf(var.first.data());    
    reader->AddVariable(var.first.data(),&MVAvarvals[var_counter]);
    var_counter++;
  }

  // add spectators
  for(const auto& var : configtree.get_child("tasks." + factory_name + ".spectators")){
    tree->SetBranchStatus(var.first.data(),1);
    var_leaves[var_counter] = tree->GetLeaf(var.first.data());    
    reader->AddSpectator(var.first.data(),&MVAvarvals[var_counter]);
    var_counter++;
  }

  reader->BookMVA("myMVA",options.get<std::string>("workdir") + "/" + options.get<std::string>("weightfile"));

  //make friend tree. we'll add the tree later as friend to `tree`, so that one has direct access to the new variables via the old tree later
  TFile* ff = TFile::Open(TString::Format("%s/%s",options.get<std::string>("workdir").data(),options.get<std::string>("outfilename").data()),"RECREATE");
  TTree TF(TString::Format("%s_MVAfriend",options.get<std::string>("treename").data()),TString::Format("%s_MVAfriend",options.get<std::string>("treename").data()));
  double response;
  TF.Branch(configtree.get<std::string>("response_name").data(),&response,(configtree.get<std::string>("response_name") + "/D").data());

  msg_svc.infomsg("Looping tree and calculating MVA output\n");
  auto nentries = tree->GetEntries();
  const double one_percent = std::floor(nentries/100);
  unsigned int percentage = static_cast<unsigned int>(one_percent);

  //make functor for response (so that you don't need an if statement for every event)
  std::function<double(TMVA::Reader*)> response_functor;
  if(configtree.get_optional<bool>("rarity"))
    response_functor = [] (TMVA::Reader* reader) {
      return reader->GetRarity("myMVA");
    };
  else
    response_functor = [] (TMVA::Reader* reader) {
      return reader->EvaluateMVA("myMVA");
    };

  for (decltype(nentries) evt = 0; evt < nentries; evt++){
    tree->GetEntry(evt);
    if(evt == percentage){
      percentage += one_percent;
      loadbar(static_cast<unsigned int>(evt),static_cast<unsigned int>(nentries));
    }
    for(unsigned int i = 0u; i < nvars+nspec; i++){
      MVAvarvals[i] = var_leaves[i]->GetValue(0);
    }

    response = response_functor(reader);
    TF.Fill();
  }
  std::printf("\n");

  TF.Write();
  TF.Print();
  ff->Close();

  input->cd();//write tree to the correct file
  tree->SetBranchStatus("*",true);//re-enable the branches  
  input->Close();

  delete reader;

  return 0;
}
