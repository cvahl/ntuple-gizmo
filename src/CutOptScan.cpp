/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  Rectangular cut optimisation configurable by boost property trees
*/
//ROOT
#include <TStopwatch.h>
#include <TFile.h>
#include <TString.h>
#include <TError.h>
//RooFit
#include <RooDataSet.h>
#include <RooWorkspace.h>
#include <RooRealVar.h>
#include <RooAbsPdf.h>
#include <RooGlobalFunc.h>
#include <RooMsgService.h>
//C++
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <type_traits>
#include <algorithm>
#include <limits>
#include <functional>
#include <iterator>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
//local
#include <../IOjuggler/IOjuggler.h>

namespace pt = boost::property_tree;

//this function checks if floating point values are the same (within their given precision)
template<typename T, typename R>
constexpr inline bool AreEqual(T&& f1, R&& f2, typename std::enable_if<std::is_convertible<R,T>::value, void>::type* = nullptr) {
  return (std::fabs(f1 - f2) <= std::numeric_limits<typename std::decay<T>::type>::epsilon() * std::fmax(fabs(f1), fabs(f2)));
}

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  const auto options = parse_options(argc, argv, "c:d:i:o:r:v:w:h","nonoptions: fold number (optional)",0);
  const auto verbosity = options.get<int>("verbosity");
  MessageService msg_svc("CutOptScan",static_cast<MSG_LVL>(verbosity));
  const auto dir = options.get<std::string>("workdir");
  //get ptree
  const auto configtree = get_ptree(options.get<std::string>("config"));
  //get dataset name (from -r option + fold number)
  const auto file_for_beef = options.get<std::string>("infilename");
  const auto ws_for_beef   = options.get<std::string>("wsname");
  auto ds_for_beef         = options.get<std::string>("dsname");
  auto extras              = options.get_child_optional("extraargs");
  if(extras)
   ds_for_beef += static_cast<std::string>((*extras).front().second.data());

  if(configtree.get_optional<bool>("suppress_RootInfo")){
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setStreamStatus(1,false);
  }

  //some variables that are needed for the scan
  //the nsteps variables control if you want to do a 1D scan, a scan in all variables or consecutively both
  const auto nsteps_1D      = configtree.get<unsigned int>("nsteps_1D",2);
  const auto nsteps         = configtree.get<unsigned int>("nsteps",2);
  const auto sig_range_lo   = configtree.get("sig_range_lo",0.0);
  const auto sig_range_hi   = configtree.get("sig_range_hi",0.0);
  const auto nsigma         = configtree.get("nsigma",0.0);
  const auto width_var      = configtree.get<std::string>("width_var","");
  const auto mean_var       = configtree.get<std::string>("mean_var","");
  const auto sigpdfname     = configtree.get_optional<std::string>("sig_pdf");
  const auto bkgpdfname     = configtree.get_optional<std::string>("bkg_pdf");
  const auto Nsig_var       = configtree.get_optional<std::string>("Nsig_var");
  const auto Nbkg_var       = configtree.get_optional<std::string>("Nbkg_var");
  const auto beef_verbosity = configtree.get("beef_verbosity","2");
  if(!sigpdfname || !bkgpdfname || !Nsig_var || !Nbkg_var) throw std::runtime_error("\"sig_pdf\",\"bkg_pdf\",\"Nsig_var\" or \"Nbkg_var\" not found in config file");
  auto FoM_mode = configtree.get("FoM_mode",0);
  if(0 < FoM_mode || FoM_mode > 1){
    msg_svc.warningmsg("FoM mode " + std::to_string(FoM_mode) + " not known. Setting to 0: S/sqrt(S*B)");
    FoM_mode = 0;
  }

  //safety checks
  if(!configtree.get_child_optional("vars")) throw std::runtime_error("child \"vars\" not found in config file");
  if(!configtree.get_optional<std::string>("beef_config")) throw std::runtime_error("\"beef_config\" not found in config file");

  const auto nvars = configtree.get_child("vars").size();
  //let's only do this once
  typename std::decay<decltype(nvars)>::type ivar = 0;
  typename std::decay<decltype(nsteps_1D)>::type istep;
  std::vector<std::string> cutset(nvars,"");
  //put operators and names into a vector to not read from the config file all the time
  //do this here, so put_cut_together can capture it
  std::vector<std::string>  cut_operators  (nvars,"");
  std::vector<std::string>  var_names      (nvars,"");
  for(const auto& var : configtree.get_child("vars")){
    cut_operators[ivar] = var.second.get<std::string>("operator",">");
    var_names    [ivar] = var.first;
    ivar++;
  }
  //initialize vector with best cuts for output of the scan
  std::vector<float> best_cut_values(nvars);

  //works with std::string, TString, maybe boost::string?
  auto put_cut_together1D = [&cutset,&ivar,&var_names,&cut_operators] (auto&& cut){    
    cutset[ivar] = var_names[ivar] + " " + cut_operators[ivar] + " " + static_cast<typename std::decay<decltype(cutset[0])>::type>(cut);
    typename std::decay<decltype(cutset[0])>::type cutstring = cutset[0];
    for(decltype(cutset.size()) ij = 1; ij < cutset.size(); ij++)
      cutstring += " && " + cutset[ij];
    return cutstring;
  };

  //note that this one is called with a vector of floating point variables rather than a string-like cut
  auto put_cut_together = [&cutset,&var_names,&cut_operators] (auto&& cut_vec){
    for(decltype(cutset.size()) ij = 0; ij < cutset.size(); ij++)
      cutset[ij] = var_names[ij] + " " + cut_operators[ij] + " " + std::to_string(cut_vec[ij]);
    typename std::decay<decltype(cutset[0])>::type cutstring = cutset[0];
    for(decltype(cutset.size()) ij = 1; ij < cutset.size(); ij++)
      cutstring += " && " + cutset[ij];
    return cutstring;
  };

  //lambda to call beef and calculate the FoM
  auto fit_and_get_FoM = [&] (auto&& cutstring, const std::string&& appendix) {
    //debug
    msg_svc.debugmsg(std::string(128,'*'));
    msg_svc.debugmsg("At appendix: " + appendix + " . Current cutstring:");
    msg_svc.debugmsg(cutstring);
    //replace the new cutstring with the dummy in the beef config file
    auto beef_config = get_ptree(configtree.get<std::string>("beef_config"));
    replace_stuff_in_ptree(beef_config,"INSERT_CUT_HERE",static_cast<std::string>(cutstring));
    //stuff for beef
    const std::string tmpdir = "tmp_forScan/";
    const auto tmp_config    = dir + "/" + tmpdir + "config" + appendix + ".info";
    const auto tmp_outfile   = tmpdir + "fitresults" + appendix + ".root";
    const auto plotname      = tmpdir + "plot" + appendix;
    replace_stuff_in_ptree(beef_config,"INSERT_PLOTNAME_HERE",plotname);
    pt::write_info(tmp_config.data(),beef_config);
    //call beef from command line
    auto dumpit = std::system(("build/bin/beef"
                               " -c " + tmp_config +
                               " -i " + file_for_beef +
                               " -o " + tmp_outfile +
                               " -d " + dir +
                               " -w " + ws_for_beef +
                               " -r " + ds_for_beef +
                               " -v " + beef_verbosity).data());
    //todo: use fit status
    if(dumpit != 0)msg_svc.debugmsg("beef finished with status " + std::to_string(dumpit));//to please the CI compiler
    //get FoM
    //read beef's output
    const auto tmpin = get_file(tmp_outfile,dir);
    auto tmpws = get_obj<RooWorkspace>(tmpin,"w");//"w" hardcoded in beef
    if(beef_config.get_child("observables").size() > 1)throw std::runtime_error("Sorry, can't do this scan for multidimensional fits");
    auto tmpobs = get_wsobj<RooRealVar>(tmpws,beef_config.get_child("observables").front().first);
    //make signal-range
    std::string range_name;
    if(sig_range_lo != 0 || sig_range_hi != 0){
      range_name = "SignalRange";
      tmpobs->setRange(range_name.data(),sig_range_lo,sig_range_hi);
    }
    else if(nsigma != 0 && !width_var.empty() && !mean_var.empty()){
      const auto tmpmean  = get_wsobj<RooRealVar>(tmpws,mean_var )->getVal();
      const auto tmpwidth = get_wsobj<RooRealVar>(tmpws,width_var)->getVal();
      //this range will change every time, so we need a unique name
      range_name = "SignalRange" + appendix;
      tmpobs->setRange(range_name.data(),tmpmean - nsigma*tmpwidth,tmpmean + nsigma*tmpwidth);
    }
    else throw std::runtime_error("Please configure a valid signal range");
    //get signal and background yields in the signal range
    const auto sigyield = get_wsobj<RooRealVar>(tmpws,*Nsig_var)->getVal() * get_wsobj<RooAbsPdf>(tmpws,*sigpdfname)->createIntegral(*tmpobs,RooFit::NormSet(*tmpobs),RooFit::Range(range_name.data()))->getVal();
    const auto bkgyield = get_wsobj<RooRealVar>(tmpws,*Nbkg_var)->getVal() * get_wsobj<RooAbsPdf>(tmpws,*bkgpdfname)->createIntegral(*tmpobs,RooFit::NormSet(*tmpobs),RooFit::Range(range_name.data()))->getVal();
    const auto current_fom = FoM_mode == 0 ? sigyield/sqrt(sigyield+bkgyield) : sigyield/sqrt(sigyield+bkgyield)*sigyield/(sigyield+bkgyield);
    msg_svc.debugmsg(TString::Format("%-20.20s: %s","Nsig in range",  std::to_string(sigyield).data()));
    msg_svc.debugmsg(TString::Format("%-20.20s: %s","Nbkg in range",  std::to_string(bkgyield).data()));
    msg_svc.debugmsg(TString::Format("%-20.20s: %s","Figure of Merit",std::to_string(current_fom).data()));
    //very important to close the file. could result in a segfault for big scans (see: https://sft.its.cern.ch/jira/si/jira.issueviews:issue-html/ROOT-3785/ROOT-3785.html)
    tmpin->Close();
    delete tmpin;
    return current_fom;
  };

  //initialize vectors needed in this scope
  ivar  = 0;
  istep = 0;
  std::vector<float>        cut_distances  (nvars,0.0);
  std::vector<float>        loosest_cuts   (nvars,0.0);
  std::vector<unsigned int> ncuts          (nvars,0u);
  //fill vector with loosest cuts and check if "vars" is sane
  for(const auto& var : configtree.get_child("vars")){
    //safety checks
    if(!var.second.get_optional<float>("loosest_cut")) throw std::runtime_error("\"loosest_cut\" in child \"vars\" not found in config file");
    if(!var.second.get_optional<float>("cut_distance_1D") || var.second.get_optional<float>("cut_distance"))
      throw std::runtime_error("\"cut_distance_1D\" or \"cut_distance\" in child \"vars\" not found in config file");
    //get values
    cut_distances[ivar] = var.second.get<float>("cut_distance_1D",0.0);
    loosest_cuts [ivar] = var.second.get<float>("loosest_cut");
    ncuts        [ivar] = var.second.get<unsigned int>("ncuts_1D",5);
    cutset       [ivar] = var_names[ivar] + " " + cut_operators[ivar] + " " + std::to_string(var.second.get<float>("start_cut_1D",loosest_cuts[ivar]));
    if(ncuts[ivar] < 3){
      msg_svc.warningmsg("The number of cuts for " + var_names[ivar] + " is less than 3. Can't do a real scan with so few points. Setting ncuts to 3");
      ncuts[ivar] = 3;
    }
    ivar++;
  }

  //start to scan
  //break if maximum number of steps is reached or if FoM limit is reached
  while(istep < nsteps_1D){
    ivar = 0;
    //a vector of cutresults for each variable
    std::vector<std::vector<std::pair<float,float> > > scan_results_1D(nvars);
    //loop variables, build cut string, replace in beef config file
    for(const auto& var : configtree.get_child("vars")){
      //do the scan
      for(typename std::decay<decltype(ncuts[ivar])>::type icut = 0; icut < ncuts[ivar]; icut++){
        const auto cutvalue = loosest_cuts[ivar] + icut*cut_distances[ivar];

        //call lambdas above and use their result directly to push the pair into the scan_results vector
        auto appendix = "1D_" + std::to_string(istep) + "_" + std::to_string(ivar) + "_" + std::to_string(icut);
        if(extras) appendix += "_f" + static_cast<std::string>((*extras).front().second.data());
        scan_results_1D[ivar].emplace_back(cutvalue,fit_and_get_FoM(put_cut_together1D(std::to_string(cutvalue)),std::move(appendix)));
      }

      //iterate results and find maximum FoM. Extend scan if maximum is at a "corner" cutvalue
      auto best_pair = *std::max_element(scan_results_1D[ivar].begin(),scan_results_1D[ivar].end(),[](const auto& res_a, const auto& res_b){return res_a.second < res_b.second;});

      //we have the best FoM from this scan, but it could be that the actual best lies outside the configured range.
      //an indicator for this is that the best FoM is at the loosest or tightest cut. if this happens the range will be extended if possible
      auto tightest_cut = loosest_cuts[ivar] + (ncuts[ivar]-1)*cut_distances[ivar];
      auto extension_counter = 0u;
      while(AreEqual(best_pair.first,tightest_cut) && ncuts[ivar] > 1){//AreEqual for floting point precision errors
        msg_svc.infomsg("Best FoM at tightest cut. Extending cut-range one step.");
        const auto cutvalue = tightest_cut + cut_distances[ivar];
        auto appendix = "1D_" + std::to_string(istep) + "_" + std::to_string(ivar) + "_" + std::to_string(ncuts[ivar]+extension_counter);
        if(extras) appendix += "_f" + static_cast<std::string>((*extras).front().second.data());
        scan_results_1D[ivar].emplace_back(cutvalue,fit_and_get_FoM(put_cut_together1D(std::to_string(cutvalue)),std::move(appendix)));
        best_pair = *std::max_element(scan_results_1D[ivar].begin(),scan_results_1D[ivar].end(),[](const auto& res_a, const auto& res_b){return res_a.second < res_b.second;});
        tightest_cut = cutvalue;
      }
      while(AreEqual(best_pair.first,loosest_cuts[ivar])){//AreEqual for floting point precision errors
        if(istep == 0 || best_pair.first <= var.second.get<float>("loosest_cut")){
          msg_svc.warningmsg("Best FoM for variable " + var_names[ivar] + " found at loosest cut " + std::to_string(loosest_cuts[ivar]) + ".");
          msg_svc.warningmsg("Can't go looser than that, please consider to apply a looser cut in the pre-selection.");
          msg_svc.warningmsg("To continue the scan, the variable will be fixed at this value (only in 1D scan).");
          ncuts[ivar] = 1;
          break;
        }
        msg_svc.infomsg("Best FoM at loosest cut. Extending cut-range down one step.");
        const auto cutvalue = loosest_cuts[ivar] - cut_distances[ivar];
        auto appendix = "1D_" + std::to_string(istep) + "_" + std::to_string(ivar) + "_" + std::to_string(ncuts[ivar]+extension_counter);
        if(extras) appendix += "_f" + static_cast<std::string>((*extras).front().second.data());
        scan_results_1D[ivar].emplace_back(cutvalue,fit_and_get_FoM(put_cut_together1D(std::to_string(cutvalue)),std::move(appendix)));
        best_pair = *std::max_element(scan_results_1D[ivar].begin(),scan_results_1D[ivar].end(),[](const auto& res_a, const auto& res_b){return res_a.second < res_b.second;});
        loosest_cuts[ivar] = cutvalue;
      }

      //we now have the best FoM and need to set "cutstring" to best cut here (to be used for the next variable or step)
      cutset[ivar] = var_names[ivar] + " " + cut_operators[ivar] + " " + std::to_string(best_pair.first);
      //push the best cuts also into a vector that will be used to fill the output (it will be overwritten nstep times, but that shouldn't be a problem)
      best_cut_values[ivar] = best_pair.first;
      //some debugging
      msg_svc.debugmsg("Finished with " + var_names[ivar] + ". Currently the best cut reads:");
      msg_svc.debugmsg(put_cut_together1D(std::to_string(best_pair.first)));

      //check if this cut is still part of the scan
      if(ncuts[ivar] > 1){
        //now we prepare the next step
        //set distance and loosest cut for the next step. e.g. like this (ncuts = 5, nintermediate = 2) :
        //         loosest                              best                              tightest
        //step 0:   cut0..............cut1..............cut2..............cut3..............cut4
        //                         new loosest                best     new tightest
        //step 1:                     cut0..cut1..cut2..cut3..cut4..cut5..cut6
        loosest_cuts[ivar]  = best_pair.first - cut_distances[ivar];
        const auto ninter   = var.second.get<unsigned int>("nintermediate_1D",1);
        cut_distances[ivar] = cut_distances[ivar]/(ninter+1);
        ncuts        [ivar] = 2*ninter + 3;
      }

      scan_results_1D[ivar].clear();
      ivar++;
    }
    istep++;
  }

  //preparations for scan in all dimensions
  //reset counters for scan in all dimensions
  istep = 0;
  ivar = 0;
  //fill some of the variables again
  for(const auto& var : configtree.get_child("vars")){
    //get values
    ncuts[ivar] = var.second.get<unsigned int>("ncuts",5);
    if(ncuts[ivar] < 3){
      msg_svc.warningmsg("The number of cuts for " + var_names[ivar] + " is less than 3. Can't do a scan with so little points. Fixing this cut!");
      ncuts[ivar] = 1;
    }
    if(var.second.get_optional<float>("cut_distance") && nsteps_1D > 0){
      cut_distances[ivar] = var.second.get<float>("cut_distance");
      loosest_cuts[ivar] = best_cut_values[ivar] - 0.5*cut_distances[ivar]*static_cast<float>(ncuts[ivar]-1);
    }//if not, the scan will use the previous step size (set up before the 1D scan)
    if(var.second.get_optional<float>("start_cut"))
      cutset[ivar] = var_names[ivar] + " " + cut_operators[ivar] + " " + std::to_string(var.second.get<float>("start_cut"));
    if(ncuts[ivar] == 1)
      msg_svc.infomsg("This cut has been fixed:" + cutset[ivar]);
    ivar++;
  }
  //define this guy here, so we don't fit the same cuts multiple times
  std::vector<std::pair<std::vector<float>,float> > scan_results;

  //start the full scan
  while(istep < nsteps){
    std::vector<std::vector<float> > individual_cuts(nvars);
    for(ivar = 0; ivar < nvars; ivar++)
      for(typename std::decay<decltype(ncuts[ivar])>::type icut = 0; icut < ncuts[ivar]; icut++)
        individual_cuts[ivar].push_back(loosest_cuts[ivar] + icut*cut_distances[ivar]);

    //we need these vectors for building all cut combinations
    std::vector<typename std::decay<decltype(ncuts[ivar])>::type> indices(nvars,0);
    std::vector<float> tmp_cuts(nvars);

    //lambda to get the results from the scan (will be used in nominal scan and in expansion if one of the cuts is at limit)
    auto get_results_for_scan = [&] (const std::string& ext = "") {
      //build appendix and cuts for the fit
      auto appendix = ext + "_" + std::to_string(istep);
      for(decltype(ivar) ijvar = 0; ijvar < nvars; ijvar++){
        tmp_cuts[ijvar] = individual_cuts[ijvar][indices[ijvar]];
        appendix += "_" + std::to_string(indices[ijvar]);
      }
      //check if the set of cuts has already been added to scan_results and return without fitting
      if(std::end(scan_results) != std::find_if(scan_results.begin(),scan_results.end(),[&tmp_cuts](const auto& isr) -> bool {
                                                bool same = true;
                                                for(size_t ijvar = 0; ijvar < tmp_cuts.size(); ijvar++)
                                                same &= AreEqual(isr.first[ijvar],tmp_cuts[ijvar]);
                                                return same;})){
        msg_svc.debugmsg("Found cutset ");
        msg_svc.debugmsg(put_cut_together(tmp_cuts));
        msg_svc.debugmsg("in scan_results. Skipping fit.");
        return;
      }

      if(extras) appendix += "_f" + static_cast<std::string>((*extras).front().second.data());
      scan_results.emplace_back(tmp_cuts,fit_and_get_FoM(put_cut_together(tmp_cuts),std::move(appendix)));
    };

    //Horrible recursive function. It works like an odometer but in different direction. So e.g.: 0 0 -> 1 0 -> 0 1 -> 1 1 (indices of each iteration for 2 variables with 2 cuts each)
    //The key is to start this function with the last variable. It will then start at the loosest cut of this variable, go to the previous variable etc. until the 0th variable is reached.
    //When the 0th variable is reached, the cut is assembled (by filling tmp_cuts correctly) and fit_and_get_FoM is called. The FoM is pushed back to scan_results by calling the lambda above.
    //As soon as all cuts of the 0th variable have been processed, the next cut of the 1st variable is used and the cuts of the 0th get iterated once more.
    //This continues until all the inner loops ncuts[nvars-1]-1 have been iterated.
    std::function<void(decltype(ivar)&&)> scan = [&] (decltype(ivar)&& lamda_ivar) -> void {
      for(typename std::decay<decltype(ncuts[lamda_ivar])>::type icut = 0; icut < ncuts[lamda_ivar]; icut++){
        indices[lamda_ivar] = icut;
        if(lamda_ivar == 0)
          get_results_for_scan();
        else
          scan(lamda_ivar-1);
      }
    };
    //call it!
    scan(nvars-1);

    //get the best FoMs
    auto best_pair = *std::max_element(scan_results.begin(),scan_results.end(),[](const auto& res_a, const auto& res_b){return res_a.second < res_b.second;});

    //play extension game as in 1D case. this time it's a bit more difficult
    //function to set indices-vector to best cut
    auto get_best_indices = [&ivar,&nvars,&indices,&individual_cuts,&best_pair,&msg_svc,&put_cut_together] () {
      decltype(indices) best_indices(nvars,0);
      for(decltype(ivar) ijvar = 0; ijvar < nvars; ijvar++){
        best_indices[ijvar] = std::distance(individual_cuts[ijvar].begin(),
                                            std::find_if(individual_cuts[ijvar].begin(),individual_cuts[ijvar].end(),
                                                         [&best_pair,&ijvar](const auto& ic) -> bool {return AreEqual(ic,best_pair.first[ijvar]);}));
      }
      //debugging
      if(msg_svc.GetMsgLvl() == MSG_LVL::DEBUG){
        std::string bis;
        for(const auto& index : best_indices)
          bis += std::to_string(index) + " ";
        msg_svc.debugmsg("Best indices are " + bis);
      }
      return best_indices;
    };

    //recursive function for the extension
    std::function<void(decltype(ivar)&&,const decltype(indices)&)> extension_scan = [&] (decltype(ivar)&& lambda_ivar, const decltype(indices)& best_indices) -> void {
      int icut = -1;
      //scan only current and extended cutvalue if this cut is at the lower or upper limit of the previous scan-range
      if(best_indices[lambda_ivar] == 0 || best_indices[lambda_ivar] == ncuts[lambda_ivar] - 2){
        msg_svc.debugmsg("Variable " + var_names[lambda_ivar] + " at range limit.");
        icut = 0;
      }
      for(;icut <= 1; icut++){
        indices[lambda_ivar] = icut + best_indices[lambda_ivar];
        if(lambda_ivar == 0)
          get_results_for_scan("EX");//the index combination could potentially (for hitting lower limits) overwrite stuff
        else
          extension_scan(lambda_ivar-1,best_indices);
      }
    };

    //initialize best_indices for extension
    auto best_indices = get_best_indices();

    //check if some of the variables reached the limit of the given scan-range. take actions if this happened
    auto at_range_limit = [&] () {
      bool hit_limits = false;
      for(ivar = 0; ivar < nvars; ivar++){
        if(best_indices[ivar] == ncuts[ivar] - 1 && ncuts[ivar] > 1){
          hit_limits = true;
          //in principle loosest_cuts could have been modified in the if() body below. but this is pathological
          const auto cutvalue = loosest_cuts[ivar] + ncuts[ivar]*cut_distances[ivar];
          msg_svc.infomsg("Best FoM for " + var_names[ivar] + " at tightest cut. Adding " + var_names[ivar] + " " + cut_operators[ivar] + " " + std::to_string(cutvalue) + " to the scan.");
          individual_cuts[ivar].push_back(cutvalue);
          //"burn" ncuts for the extension. it's not used further
          ncuts[ivar] += 1;
        }
        if(best_indices[ivar] == 0 && ncuts[ivar] > 1){
          if(best_pair.first[ivar] <= (*std::next(configtree.get_child("vars").begin(),ivar)).second.get<float>("loosest_cut")){
            msg_svc.warningmsg("Best FoM for variable " + var_names[ivar] + " found at loosest cut " + std::to_string(loosest_cuts[ivar]) + ".");
            msg_svc.warningmsg("Can't go looser than that, please consider to apply a looser cut in the pre-selection.");
            msg_svc.warningmsg("To continue the scan, the variable will be fixed at this value.");
            ncuts[ivar] = 1;
            continue;
          }
          else{
            hit_limits = true;
            const auto cutvalue = loosest_cuts[ivar] - cut_distances[ivar];
            msg_svc.infomsg("Best FoM for " + var_names[ivar] + " at loosest cut. Adding " + var_names[ivar] + " " + cut_operators[ivar] + " " + std::to_string(cutvalue) + " to the scan.");
            individual_cuts[ivar].insert(individual_cuts[ivar].begin(),cutvalue);//careful: best_indices still 0, but not indexing the correct element any more now. This is fine for the extension
            ncuts[ivar] += 1;
            loosest_cuts[ivar] = cutvalue;
          }
        }
      }
      return hit_limits;
    };

    while( at_range_limit() ){
      //we don't need to scan the full cut space again, so we extend in ivar (set this element to 1) and look at the best and their neighboring points (set elements to 3)
      //this is done in extension_scan. start it like scan above
      extension_scan(nvars-1,best_indices);
      //set best_pair and best_indices to their new values
      best_pair = *std::max_element(scan_results.begin(),scan_results.end(),[](const auto& res_a, const auto& res_b){return res_a.second < res_b.second;});
      best_indices = get_best_indices();
    }

    //some debugging
    msg_svc.debugmsg("Finished with step " + std::to_string(istep) + " of the nvars-dimensional scan . Currently the best cut reads:");
    msg_svc.debugmsg(put_cut_together(best_pair.first));
    msg_svc.debugmsg("at a figure of merit of: " + std::to_string(best_pair.second));

    //preparation of next step. for details have a look at the 1D case above
    ivar = 0;
    for(const auto& var : configtree.get_child("vars")){
      if(ncuts[ivar] > 1){//check if cut has been fixed by the user or the algoritm
        loosest_cuts[ivar]  = best_pair.first[ivar] - cut_distances[ivar];
        const auto ninter   = var.second.get<unsigned int>("nintermediate",1);
        cut_distances[ivar] = cut_distances[ivar]/(ninter+1);
        ncuts        [ivar] = 2*ninter + 3;
      }
      ivar++;
    }
    //put cutvalues of best FoMs in matrix
    if(istep + 1 == nsteps){
      for(ivar = 0; ivar < nvars; ivar++)
        best_cut_values[ivar] = best_pair.first[ivar];
    }
    istep++;
  }

  //write output files
  if(extras)
    msg_svc.infomsg("Best cutset in fold " + static_cast<std::string>((*extras).front().second.data()));
  else
    msg_svc.infomsg("Best cutset");
  const auto best_cutstring = put_cut_together(best_cut_values);
  msg_svc.infomsg(best_cutstring);

  std::ofstream TCutout;
  if(extras){
    TCutout.open((dir + "/" + options.get<std::string>("outfilename") + static_cast<std::string>((*extras).front().second.data()) + ".info").data());
    for(decltype(best_cut_values.size()) ij = 0; ij < best_cut_values.size(); ij++){
      TCutout << (var_names[ij] + " " + cut_operators[ij] + " " + std::to_string(best_cut_values[ij])).data() << std::endl;
    }
  }
  else{
    TCutout.open((dir + "/" + options.get<std::string>("outfilename") + ".info").data());
    TCutout << best_cutstring.data();
  }
  TCutout.close();

  clock.Stop();
  clock.Print();
  return 0;
}
