//C++
#include <type_traits>//decltype
#include <vector>
#include <exception>
#include <string>
//root
#include <TObject.h>
#include <TFile.h>
#include <TChain.h>
#include <TString.h>
#include <TTree.h>
#include <TFriendElement.h>
#include <TObjArray.h>
#include <TObjString.h>
//RooFit
#include <RooDataSet.h>
#include <RooWorkspace.h>
#include <RooRealVar.h>
#include <RooLinkedList.h>
#include <RooCmdArg.h>
//local
#include <../IOjuggler/IOjuggler.h>

#ifdef NTUPLE_GIZMO_GIT_HASH
  #define NTUPLE_GIZMO_HASH NTUPLE_GIZMO_GIT_HASH
#else
  #define NTUPLE_GIZMO_HASH " "
#endif

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "c:d:i:o:t:r:v:h","nonoptions: any number of <file:friendtree> combinations");
  MessageService msg_svc("stuff2hist",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msg_svc.infomsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_HASH));
  auto configtree = get_ptree(options.get<std::string>("config"));
  auto wd = options.get<std::string>("workdir");
  auto fn = options.get<std::string>("infilename");
  auto tn = options.get<std::string>("treename");

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msg_svc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  //create output file
  TFile of((wd + "/" + options.get<std::string>("outfilename")).data(),"RECREATE");

  //lambda to make histos from TTree and inheriting objects
  auto draw_from_tree = [&configtree,&of,&msg_svc] (auto& tree) {
    of.cd();
    for (const auto& hist : configtree.get_child("hists")){
      msg_svc.debugmsg("at " + hist.first);
      if(msg_svc.GetMsgLvl() == MSG_LVL::DEBUG)print_ptree(hist.second);
      if(hist.second.get_optional<std::string>("Draw"))
        tree.Draw(hist.second.get<std::string>("Draw").data(),
                  hist.second.get<std::string>("cut","").data(),
                  hist.second.get<std::string>("opt","").data(),
                  hist.second.get("nentries",TTree::kMaxEntries),
                  hist.second.get("firstentry",0));
      else if(hist.second.get_optional<std::string>("var")){
        //put all the options into a TTree::Draw() string
        TString dc = hist.second.get<std::string>("var");
        if(hist.second.get_optional<std::string>("yvar")){
          dc += ":" + hist.second.get<std::string>("yvar");
          if(hist.second.get_optional<std::string>("zvar"))
            dc += ":" + hist.second.get<std::string>("zvar");
        }
        dc += ">>" + hist.second.get("histname",hist.second.get<std::string>("var"));
        if(hist.second.get_optional<std::string>("nbins")){
          dc += "(" + hist.second.get<std::string>("nbins") + "," + hist.second.get<std::string>("min") + "," + hist.second.get<std::string>("max");
          //if nbins is specified, nbinsy/z have to be given as well if these dimensions exist
          if(hist.second.get_optional<std::string>("yvar")){
            dc += hist.second.get<std::string>("nbinsy") + "," + hist.second.get<std::string>("ymin") + "," + hist.second.get<std::string>("ymax");
            if(hist.second.get_optional<std::string>("zvar"))
              dc += hist.second.get<std::string>("nbinsz") + "," + hist.second.get<std::string>("zmin") + "," + hist.second.get<std::string>("zmax");
          }
        }
        dc += ")";
        //well, that was easy... Now draw:
        tree.Draw(dc.Data(),
                  hist.second.get<std::string>("cut","").data(),
                  hist.second.get<std::string>("opt","").data(),
                  hist.second.get("nentries",TTree::kMaxEntries),
                  hist.second.get("firstentry",0));
      }
    }
    of.Write();
  };

  //if the input is a TChain, strange stuff needs to be done:
  if(fn.find("*") != std::string::npos || fn == "Chain"){
    TChain ch(tn.data(),"Chain");
    msg_svc.infomsg("Request to save histograms defined in " + options.get<std::string>("config") +
                    " from TChain " + tn + " to " + wd + "/" + options.get<std::string>("outfilename"));
    //add the files depending on what was provided as -i option
    if(fn == "Chain"){
      if(!configtree.get_child_optional("files")) throw std::runtime_error("Used \"Chain\" as filename but did not provide \"files\" node in config file");
      for(const auto& filename : configtree.get_child("files"))
        ch.Add(filename.first.data());
    }
    else ch.Add((fn).data());
    draw_from_tree(ch);
  }
  //otherwise get file and object
  else{
    auto input = get_file(fn,wd);
    auto obj = get_obj<TObject>(input,tn);
    //is it a RooWorkspace?
    if(obj->InheritsFrom("RooWorkspace")){
      auto ws = static_cast<RooWorkspace*>(obj);
      auto tempds = get_wsobj<RooDataSet>(ws,options.get<std::string>("dsname"));
      msg_svc.infomsg("Request to save histograms defined in " + options.get<std::string>("config") +
                      " from RooDataSet " + options.get<std::string>("dsname") + " in RooWorkspace " + tn +
                      " in " + wd + "/" + fn + " to " + wd + "/" + options.get<std::string>("outfilename"));

      //lambda to make histos from RooDataSets
      auto draw_from_ds = [&configtree,&of,ws,&msg_svc] (const RooDataSet& ds) {

        of.cd();
        for (const auto& hist : configtree.get_child("hists")){
          msg_svc.debugmsg("at " + hist.first);
          if(msg_svc.GetMsgLvl() == MSG_LVL::DEBUG)print_ptree(hist.second);

          auto copied_ds = new RooDataSet(ds,"copy");
          RooDataSet* dummy = nullptr;
          RooRealVar* dummy_y = nullptr;
          RooRealVar* dummy_z = nullptr;
          //needed for custom binning
          RooLinkedList llist;
          std::vector<RooCmdArg*> args;

          if(hist.second.get_optional<std::string>("cut")){
            msg_svc.debugmsg("requested a cut on a RooDataSet: " + hist.second.get<std::string>("cut"));
            dummy = static_cast<RooDataSet*>(copied_ds->reduce(hist.second.get<std::string>("cut").data()));
            copied_ds = dummy;//this works only by pointer
          }
          if(hist.second.get_optional<std::string>("weight")){
            msg_svc.debugmsg("requested weighting the RooDataSet with: " + hist.second.get<std::string>("weight"));
            dummy = new RooDataSet(copied_ds->GetName(),copied_ds->GetTitle(),copied_ds,*copied_ds->get(),"",hist.second.get<std::string>("weight").data());
            msg_svc.debugmsg(TString::Format("N sweighted entries of %s: %.2f",copied_ds->GetName(),dummy->sumEntries()));
            copied_ds = dummy;
          }
          if (hist.second.get_optional<std::string>("Draw")){
            TString dc = hist.second.get<std::string>("Draw");
            auto ndim = dc.Tokenize(":")->GetEntries();
            msg_svc.debugmsg("Drawing " + std::to_string(ndim) + "D histogram");
            std::vector<TString> varnames(ndim,"");
            //draw command looks like mLb:mD0:mLc>>htemp(...) - note the order: z:y:x
            //separate at each ":" and cut off a possible ">>"
            for(decltype(ndim) vi = 0; vi < ndim; vi++ )
              varnames[vi] = static_cast<TObjString*>(static_cast<TObjString*>(dc.Tokenize(":")->At(ndim-vi-1))->String().Tokenize(">>")->At(0))->String();
            auto var = get_wsobj<RooRealVar>(ws,varnames[0]);

            TString histname = varnames[0];
            std::vector<int> bins;
            std::vector<double> mins,maxs;
            //get last variable. the divided string could now look like mLc>>(...). Tokenize for ">>", and count the subdivided strings
            TObjArray* temp = static_cast<TObjString*>(dc.Tokenize(":")->At(dc.Tokenize(":")->GetEntries()-1))->String().Tokenize(">>");
            if( temp->GetEntries() == 2 ){
              TObjArray* temp1 = static_cast<TObjString*>(temp->At(1))->String().Tokenize("(");
              histname = static_cast<TObjString*>(temp1->At(0))->String();
              if( temp1->GetEntries() == 2 ){
                //this determines the binning
                TObjArray* temp2 = static_cast<TObjString*>(temp1->At(1))->String().Tokenize(",");
                if(temp2->GetEntries() != ndim*3) throw std::runtime_error("binning of draw string faulty");
                for(auto ib = 0; ib < ndim; ib++){
                  bins.push_back(static_cast<TObjString*>(temp2->At(3*ib))->String().Atoi());
                  mins.push_back(static_cast<TObjString*>(temp2->At(3*ib+1))->String().Atof());
                  maxs.push_back(static_cast<TObjString*>(temp2->At(3*ib+2))->String().Atof());//the closing bracket is ignored by Atof
                }
              }
            }
            //put it all together
            if(!bins.empty()) args.push_back( new RooCmdArg(RooFit::Binning(bins[0],mins[0],maxs[0])));
            if(ndim > 1){
              dummy_y = get_wsobj<RooRealVar>(ws,varnames[1]);
              if(!bins.empty()) args.push_back( new RooCmdArg(RooFit::YVar(*dummy_y,RooFit::Binning(bins[1],mins[1],maxs[1]))));
              else args.push_back( new RooCmdArg(RooFit::YVar(*dummy_y)));
              if(ndim > 2){
                dummy_z = get_wsobj<RooRealVar>(ws,varnames[2]);
                if(!bins.empty()) args.push_back( new RooCmdArg(RooFit::ZVar(*dummy_z,RooFit::Binning(bins[2],mins[2],maxs[2]))));
                else args.push_back( new RooCmdArg(RooFit::ZVar(*dummy_z)));
              }
            }
            for (auto arg : args){
              //need to set recursive arguments to true, so that the binning within YVar and ZVar is taken into account
              arg->setProcessRecArgs(true,true);
              llist.Add(arg);
            }
            copied_ds->createHistogram("",*var,llist);
            //no, of course the name of hist is not what you would type as name in the line above ...
            TString roofit_histname = "_";
            for(const auto& vn : varnames)
              roofit_histname += "_" + vn;
            static_cast<TNamed*>(gDirectory->Get(roofit_histname.Data()))->SetName(histname.Data());
          }
          else if(hist.second.get_optional<std::string>("var")){
            auto var = get_wsobj<RooRealVar>(ws,hist.second.get<std::string>("var"));
            if(hist.second.get_optional<std::string>("nbins"))
              args.push_back( new RooCmdArg(RooFit::Binning(std::stoi(hist.second.get<std::string>("nbins")),
                                                            std::stod(hist.second.get<std::string>("min")),
                                                            std::stod(hist.second.get<std::string>("max")))));
            if(hist.second.get_optional<std::string>("yvar")){
              dummy_y = get_wsobj<RooRealVar>(ws,hist.second.get<std::string>("yvar"));
              if(hist.second.get_optional<std::string>("nbinsy"))
                args.push_back( new RooCmdArg(RooFit::YVar(*dummy_y,RooFit::Binning(std::stoi(hist.second.get<std::string>("nbinsy")),
                                                                                    std::stod(hist.second.get<std::string>("ymin")),
                                                                                    std::stod(hist.second.get<std::string>("ymax"))))));
              else
                args.push_back( new RooCmdArg(RooFit::YVar(*dummy_y)));
              if(hist.second.get_optional<std::string>("zvar")){
                dummy_z = get_wsobj<RooRealVar>(ws,hist.second.get<std::string>("zvar"));
                if(hist.second.get_optional<std::string>("nbinsz"))
                  args.push_back( new RooCmdArg(RooFit::ZVar(*dummy_z,RooFit::Binning(std::stoi(hist.second.get<std::string>("nbinsz")),
                                                                                      std::stod(hist.second.get<std::string>("zmin")),
                                                                                      std::stod(hist.second.get<std::string>("zmax"))))));
                else
                  args.push_back( new RooCmdArg(RooFit::ZVar(*dummy_z)));
              }
            }
            for (auto arg : args){
              //need to set recursive arguments to true, so that the binning within YVar and ZVar is taken into account
              arg->setProcessRecArgs(true,true);
              llist.Add(arg);
            }
            copied_ds->createHistogram("",*var,llist);
            //no, of course the name of hist is not what you would type as name in the line above ...
            TString roofit_histname = "__" + hist.second.get<std::string>("var");
            if(auto yvarname = hist.second.get_optional<std::string>("yvar"))
              roofit_histname += "_" + *yvarname;
            if(auto zvarname = hist.second.get_optional<std::string>("zvar"))
              roofit_histname += "_" + *zvarname;
            static_cast<TNamed*>(gDirectory->Get(roofit_histname.Data()))->SetName(hist.second.get("histname",hist.second.get<std::string>("var")).data());
          }
          //cleanup
          for (auto arg : args)
            delete arg;
          args.clear();
          delete copied_ds;
        }
        of.Write();
      };

      if(configtree.get_optional<std::string>("weight")){
        RooDataSet ds(tempds->GetName(),tempds->GetTitle(),tempds,*tempds->get(),"",configtree.get<std::string>("weight").data());
        msg_svc.debugmsg(TString::Format("N sweighted entries of %s: %.2f",tempds->GetName(),ds.sumEntries()));
        draw_from_ds(ds);
      }
      else draw_from_ds(*tempds);
    }
    //no? is it a TTree?
    else if (obj->InheritsFrom("TTree")){
      msg_svc.infomsg("Request to save histograms defined in " + options.get<std::string>("config") +
                      " from TTree " + tn + " in " + wd + "/" + fn + " to " + wd + "/" + options.get<std::string>("outfilename"));
      auto tree = static_cast<TTree*>(obj);
      //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
      //and because they will go out of scope, we have to push them into a vector
      std::vector< TFriendElement* > fes;
      if(options.get_child_optional("extraargs")){
        for(const auto& ea : options.get_child("extraargs")){
          auto ff = get_file(static_cast<std::string>(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(0)))->String().Data()),wd);
          fes.push_back(tree->AddFriend(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(1)))->String().Data(),ff));
        }
      }
      draw_from_tree(*tree);
    }
    //still no? ok, no idea what it is then...
    else throw std::runtime_error("object in inputfile is neiter RooWorkspace nor TTree");
  }
  return 0;
}
