#ifndef MISID_BETA_H
#define MISID_BETA_H

#include <vector>
#include <type_traits>
#include <functional>
#include <algorithm>

#include <TTree.h>
#include <TBranch.h>
#include <TLorentzVector.h>

typedef std::vector<double> dvec;
typedef std::vector<unsigned int> uivec;
typedef std::vector<std::function<double(std::vector<TLorentzVector>&)>> BetaFunctors;

//function to add branches to tree
TBranch* to_tree(TTree* tree, uivec& cis, std::string&& name, dvec& mbs){
  mbs.push_back(0.f);
  for(const auto& idx : cis)
    name += std::to_string(idx);
  return tree->Branch(name.data(),&mbs.back(),(name+"/D").data());
}

//add functor for betas
void beta_functor(BetaFunctors& bfs, uivec& cis){
  //very important to make a copy of cis instead of capturing by reference
  bfs.emplace_back( [cis] (std::vector<TLorentzVector>& lvs) -> double {
    double p0 = lvs[cis[0]-1].P(), beta_stub = 0;
    for(decltype(cis.size()) i = 1; i < cis.size(); i++)
      beta_stub += lvs[cis[i]-1].P();
    return (beta_stub-p0)/(beta_stub+p0);
  });
}

//add functor for invariant masses
void mass_functor(BetaFunctors& bfs, uivec& cis){
  bfs.emplace_back( [cis] (std::vector<TLorentzVector>& lvs) -> double {
    TLorentzVector temp;
    for(const auto& idx : cis)
      temp += lvs[idx-1];
    return temp.M();
  });
}

template <typename I>
std::vector<TBranch*> misID_beta(I&& n_daughters, dvec& mbs, TTree* tree, BetaFunctors& bfs){
  std::vector<TBranch*> branches;
  //iterate all possible n_body combinations
  for(unsigned int n_body = static_cast<unsigned int>(n_daughters); n_body > 1; n_body--){
    uivec combination_indices(n_body,0);
    for(decltype(combination_indices.size()) i = 0; i < combination_indices.size(); i++)
      combination_indices[i] = i+1;
    combinatorics(n_body,n_daughters,mbs,combination_indices,0,tree,bfs,branches);
  }
  return std::move(branches);
}

//simple function to get all relevant permutations for beta of (n>3)-body combinations
void permutations(dvec& mbs, uivec& cis, TTree* tree, BetaFunctors& bfs,
                                    std::vector<TBranch*>& branches){
  for(decltype(cis.size()) i = 0; i < cis.size(); i++){
    branches.push_back(to_tree(tree,cis,"beta_",mbs));
    beta_functor(bfs,cis);
    //move first element to the end (just convention to write the rotation afterwards.)
    //http://en.cppreference.com/w/cpp/algorithm/rotate
    std::rotate(cis.begin(),cis.begin()+1,cis.end());
  }
}

//construct all possible n_body combinations given n_daughters
//the idea to construct the combinations is that the combination indices are never the same number
//and rise with the index number. to achieve this, the last index gets incremented until
//n_daughters is reached, then the second to last is incremented, while building all possible
//combinations with the last index etc. this makes the function recursive.
//EXAMPLE (3 body combinations for 5 final state daughters)
// 123, 124, 125, 134, 135, 145, 234, 235, 245, 345
template <typename I>
void combinatorics(unsigned int& n_body, I&& n_daughters, dvec& mbs,
                                    uivec& combination_indices, unsigned int index, TTree* tree,
                                    BetaFunctors& bfs, std::vector<TBranch*>& branches){

  //break condition ultimately fulfilled when function tries
  //to increment the first index to an invalid value
  while(combination_indices[index] <= n_daughters - n_body + index + 1){

    if(index > 0 && combination_indices[index] <= combination_indices[index-1])
      combination_indices[index] = combination_indices[index-1]+1;

    if(index < n_body-1)
      combinatorics(n_body,n_daughters,mbs,combination_indices,index+1,tree,bfs,branches);
    else if(index == n_body-1){
      branches.push_back(to_tree(tree,combination_indices,"M_",mbs));
      mass_functor(bfs,combination_indices);
      if(n_body > 2)
        permutations(mbs,combination_indices,tree,bfs,branches);
      else{
        branches.push_back(to_tree(tree,combination_indices,"beta_",mbs));
        beta_functor(bfs,combination_indices);
      }
    }
    else throw std::runtime_error("How the hell did i end up here?");

    //count up here. reset to lowest possible values before combination index would exceed maximum
    //at_max is a measure for indices being at their maximal value. if this condition is not met,
    //but the current index is at max, this index and all following are set back to their minimally
    //possible value. this value is dictated by the previous index
    bool at_max = true;
    for(decltype(combination_indices.size()) i = 0; i < combination_indices.size(); i++)
      if(combination_indices[i] < n_daughters - n_body + i + 1)
        at_max = false;
    if(combination_indices[index] == n_daughters - n_body + index + 1 && !at_max && index > 0){
      for(decltype(combination_indices.size()) i = index; i < combination_indices.size(); i++)
        combination_indices[i] = combination_indices[i-1] + 1;
      break;
    }
    else
      combination_indices[index] += 1;
  }
}
#endif // MISID_BETA_H
