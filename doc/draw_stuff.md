# Documentation for draw_stuff.cpp

This is the documentation on how to write config files for [draw_stuff](src/draw_stuff.cpp).<br>
For a quick start have a look at [an example](config/draw_stuff_example.info) or at config files used [here](https://gitlab.cern.ch/lhcb-bandq-exotics/Lb2LcD0K/tree/master/config/plotting) <br>
`draw_stuff` can be used to draw any type of objects inheriting from `TH1`, `TF1`, `TGraph2D`, `TGraph`, `TEfficiency` and `TMultiGraph` that is stored anywhere in a root file.<br>
`TH2A` is also supported but needs to be [setup correctly within cmake](#cmake-instructions-for-th2a-support).

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file
- `-d` : working directory
- `-p` : prefix
- `-v` : verbosity (1-3, optional)
- `-h` : help

# Write a config file
The config file has to have a child named `Canvas` or `Plots` to iterate objects in the file and draw them. <br>
The difference is, that the [`Canvas` iterator](#config-files-with-a-canvas-iterator) gives you full functionality including `TPads` and multiple plots per canvas/pad, 
but the name of the plots have to be given explicitly. The [`Plots` iterator](#config-files-without-a-canvas-iterator) is designed to quickly and efficiently produce plots with the same layout by using wildcards.

### Using prefix in config file
You can put a tag `{prefix}` anywhere in a node in the config file. This tag will be prelaced by whatever is passed through the `-p` command line option. This is especially useful in conjunction with 
wildcarded processing pipelines.

### Config files with a Canvas iterator
Within an iterable canvas `output_name` and `outputformats` define [how](https://root.cern.ch/doc/master/classTCanvas.html#abb7a40ea658c348cdc8f6925eb671314 "TCanvas::SaveAs") the canvas will be saved.
If they are not given `output_name` will be the name of the iterable child in `Canvas` and `outputformats` will be `.pdf`.<br>
The most important child of an iterable canvas is the ***madatory*** `Plots`. It configures all plotable objects that will be drawn to the same canvas.<br>
An iterable object in `Plots` needs a `name` and a `filename`. The plottable object `name` will be searched for in the `TFile` `filename`.<br>
All other commands are optional. This means the minimal config file for `draw_stuff` looks like this:    

    Canvas {
      my_awesome_plot {; will produce `my_awesome_plot.pdf` in the directory specified by `-d` option (N.B.: use e.g. output_name "../../my_awesome_plot" to save the plot in a path relative to the -d one)
        Plots {
          iterable_plot_0 { ;this will be the name of the plot internally. Whenever the plot is called again (e.g. in TLegend, KSTest etc.) this name has to be used
            filename "my_awesome_file.root" ; will search for `my_awesome_file.root` in the directory specified by `-d` option
            name     "my_awesome_plotable_object" ; `draw_stuff` will search for this object anywhere within `my_awesome_file.root`
          }
        }
      }
    }

An option to draw several `TPad`s in one `TCanvas` has been implemented as well. It is not tested yet, but should work like this:

    Canvas {
      my_awesome_plot {; will produce `my_awesome_plot.pdf` in the directory specified by `-d` option (N.B.: use e.g. output_name "../../my_awesome_plot" to save the plot in a path relative to the -d one)
        Pad {
          pad0 { ;the name of this node doesn't play a role
            name  "pad0" ; default: "pad"
            title "pad0" ; default: "myPad"
            xlo   0.0    ; default: 0.0
            xhi   0.5    ; default: 1.0
            ylo   0.0    ; default: 0.0
            yhi   1.0    ; default: 1.0
            Plots {
              iterable_plot_0 { ;this will be the name of the plot internally. Whenever the plot is called again (e.g. in TLegend, KSTest etc.) this name has to be used
                filename "my_awesome_file.root" ; will search for `my_awesome_file.root` in the directory specified by `-d` option
                name     "my_awesome_plotable_object" ; `draw_stuff` will search for this object anywhere within `my_awesome_file.root`
              }
            }
          }
          pad1 {
            ...
          }
        }
      }
    }

#### Optional objects in `Canvas`-iterator:
- CanvXSize        : ( `int`      , default `2048`  ) X size of canvas in pixel
- CanvYSize        : ( `int`      , default `1280`  ) Y size of canvas in pixel
- lmargin          : ( `float`    , default `0.12`  ) Left margin
- bmargin          : ( `float`    , default `0.12`  ) Bottom margin
- rmargin          : ( `float`    , default `0.01`  ) Right margin
- tmargin          : ( `float`    , default `0.01`  ) Top margin
- logx             : ( `bool`     , default `false` ) Logarithmic x-axis
- logy             : ( `bool`     , default `false` ) Logarithmic y-axis
- logz             : ( `bool`     , default `false` ) Logarithmic z-axis
- Palette          : ( `int`      , optional        ) Pick one from https://root.cern.ch/doc/master/classTColor.html or [here](include/CustomPalettes.h)
- NumberContours   : ( `int`      , optional        ) Set the default number of contour levels when drawing 2-d plots
- MaxDigits        : ( `int`      , optional        ) Set TGaxis::MaxDigits for this canvas (and the following, since it's a global variable) https://root.cern.ch/doc/master/classTGaxis.html#GA12
- HatchesLineWidth : ( `int`      , optional        ) Set gStyle->SetHatchesLineWidth(). See https://root.cern.ch/doc/master/classTAttFill.html#F2
- HatchesSpacing   : ( `int`      , optional        ) Set gStyle->SetHatchesSpacing(). See https://root.cern.ch/doc/master/classTAttFill.html#F2
- overlay          : ( `string`   , optional        ) Draw first plot again: once with original option, once with "axissame". The string given to overlay is irrelevant, unless it is "axis" (only draw first plot once with "axissame") or "all" (draw all plots again).
- ExponentOffsetX  : ( `node`     , optional        )
  - OffsetX        : ( `double`   , optional        ) Set x offset of exponential on x-axis
  - OffsetY        : ( `double`   , optional        ) Set y offset of exponential on x-axis
- ExponentOffsetY  : ( `node`     , optional        )
  - OffsetX        : ( `double`   , optional        ) Set x offset of exponential on y-axis
  - OffsetY        : ( `double`   , optional        ) Set y offset of exponential on y-axis
- APPEND           : ( ptree child, optional        ) Allows to append `value` to the value of any `key` occurence recursively (starting from the current node) in the config file.
- REPLACE          : ( ptree child, optional        ) Allows to replace all occurences of `key` by `value` recursively (starting from the current node) in the config file. Looks like:
        
        REPLACE {  or APPEND
            key value
        }

- *boxes*   : Adds TBox object to canvas. They are iterable.
    - **A list of iterable items** : They correspond to TBox objects one wants to draw.<br>
      The identifier name in the property tree is of no importance within the plotter. The following parameters of each TBox objects are configurable
        - x1        : ( `double`, no default        ) X1 position in NDC coordinates
        - x2        : ( `double`, no default        ) X2 position in NDC coordinates
        - y1        : ( `double`, no default        ) Y1 position in NDC coordinates
        - y2        : ( `double`, no default        ) Y2 position in NDC coordinates
        - FillColor        : ( `string`, default `"white"` ) Set fill color (needs to be set to change `FillColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - FillColorAlpha   : ( `double`, default `1.0`     ) Set fill color alpha
        - FillStyle        : ( `short` , default `1001`    ) Set fill style https://root.cern.ch/doc/master/classTAttFill.html#F2
        - LineColor        : ( `string`, default `"black"` ) Set Line color (needs to be set to change `LineColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C` (for TH2A this will change the color of the bin-border lines)
        - LineColorAlpha   : ( `double`, default `1.0`     ) Set Line color alpha
        - LineWidth        : ( `short` , default `1`       ) Set Line Width https://root.cern.ch/doc/master/classTAttLine.html#L2 (for TH2A this will change the with of the bin-border lines)
        - LineStyle        : ( `short` , default `1`       ) Set Line style https://root.cern.ch/doc/master/classTAttLine.html#L3

- *latexts*   : Adds TLatex object to canvas. They are iterable.
    - **A list of iterable items** : They correspond to TLatex objects one wants to draw.<br>
      The identifier name in the property tree is of no importance within the plotter. The following parameters of each TLatex objects are configurable
        - X         : ( `double`, no default        ) X position in NDC coordinates
        - Y         : ( `double`, no default        ) Y position in NDC coordinates
        - Text      : ( `string`, no default        ) Text to add. See https://root.cern.ch/doc/master/classTLatex.html
        - TextAlign : ( `int`   , default `11`      ) Align text https://root.cern.ch/doc/master/classTAttText.html#T1
        - TextAngle : ( `float` , default `0.f`     ) Set text angle https://root.cern.ch/doc/master/classTAttText.html#T2
        - TextColor : ( `string`, default `"black"` ) Set text color. All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - TextSize  : ( `float` , optional          ) Set the textsize https://root.cern.ch/doc/master/classTAttText.html#T4
        - TextFont  : ( `int`   , optional          ) Set the font https://root.cern.ch/doc/master/classTAttText.html#T5

- *legends*   : Adds TLegend object to canvas. They are iterable.
    - **A list of iterable items** : They correspond to legends one wants to draw.<br>
      The identifier name in the property tree is of no importance within the plotter. <br>
      There are two ways (labelled solution 1/2 below) to give the legends position-coordinates. One uses (xlo,xhi,ylo,yhi) - i.e. the ROOT typical NDC coordinates, the other (xlo,dright,dtop,height). See below how they are defined.<br>
      The following parameters of each legend are configurable
        - xlo            : ( `double`, no default       ) X1 position in NDC coordinates
        - xhi            : ( `double`, solution 1       ) X2 position in NDC coordinates
        - ylo            : ( `double`, solution 1       ) Y1 position in NDC coordinates
        - yhi            : ( `double`, solution 1       ) Y2 position in NDC coordinates
        - dright         : ( `double`, solution 2       ) Distance from right margin to right end of legend (text in legend can jut out)
        - dtop           : ( `double`, solution 2       ) Distance from top margin to upper end of legend
        - height         : ( `double`, solution 2       ) Height of legend in units of textsizes
        - header         : ( `string`, default `""`     ) Header of the legend
        - TextSize       : ( `double`, default `0.05`   ) Set the legend's textsize
        - TextAlign      : ( `short` , default `12`     ) Align text https://root.cern.ch/doc/master/classTAttText.html#T1
        - TextAngle      : ( `float` , default `0.f`    ) Set text angle https://root.cern.ch/doc/master/classTAttText.html#T2
        - BorderSize     : ( `int`   , default `0`      ) Set width of line around legend
        - FillColor      : ( `string`, default `"white"`) Set fill color (needs to be set to change `FillColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - FillColorAlpha : ( `double`, default `1.0`     ) Set fill color alpha
        - FillStyle      : ( `int`   , default `1001`    ) Set fill style https://root.cern.ch/doc/master/classTAttFill.html#F2
        - **A list of iterable items**: They correspond to `Plots`-iterators one wants to refer to.<br>
          The identifier name in the property tree is of no importance within the plotter. The following parameters are configurable
            - name  : ( `string`, no default      ) Name of the projection. These are the names of plotable objects. In the example above this would be `"my_awesome_plotable_object"`
            - label : ( `string`, no default      ) Label of component
            - option: ( `string`, default `"lpf"` ) Distance from top margin to upper end of legend

                    legends {
                      leg1 {
                        header   "LHCb internal"
                        xlo      0.42
                        dtop     0.05
                        height   8.0
                        dright   0.35
                        TextSize 0.05

                        MVA_Final_Train_S {
                          name   "MVA_Final_Train_S"
                          label  "Training Signal"
                          option "f"
                        }
                        MVA_Final_Train_B {
                          name   "MVA_Final_Train_B"
                          label  "Training Background"
                          option "f"
                        }
                        MVA_Final_S {
                          name   "MVA_Final_S"
                          label  "Test Signal"
                          option "epl"
                        }
                        MVA_Final_B {
                          name   "MVA_Final_B"
                          label  "Test Background"
                          option "epl"
                        }
                      }
                    }

- *lines*   : Adds TLine object to canvas. They are iterable.
    - **A list of iterable items** : They correspond to TLine objects one wants to draw.<br>
      The identifier name in the property tree is of no importance within the plotter. The following parameters of each TLine objects are configurable
        - xlo       : ( `double`, no default        ) X start position
        - xhi       : ( `double`, no default        ) X end position
        - ylo       : ( `double`, no default        ) Y start position
        - yhi       : ( `double`, no default        ) Y end position
        - style     : ( `short` , default `2`       ) Set Line style https://root.cern.ch/doc/master/classTAttLine.html#L3
        - width     : ( `short` , default `1`       ) Set Line Width https://root.cern.ch/doc/master/classTAttLine.html#L2
        - color     : ( `string`, default `"black"` ) Set Line color. All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - option    : ( `string`, default `"l"`     ) Draw Line with this option.

- *pavetexts*   : Adds TPaveText object to canvas. They are iterable.
    - **A list of iterable items** : They correspond to PaveTexts one wants to draw.<br>
      The identifier name in the property tree is of no importance within the plotter. The following parameters of each PaveText are configurable <br>
      There are two ways (labelled solution 1/2 below) to give the PaveTexts position-coordinates. One uses (xlo,xhi,ylo,yhi) - i.e. the ROOT typical NDC coordinates, the other (xlo,dright,dtop,height). See below how they are defined.<br>
      The following parameters of each PaveText are configurable
        - xlo            : ( `double`, no default       ) X1 position in NDC coordinates
        - xhi            : ( `double`, solution 1       ) X2 position in NDC coordinates
        - ylo            : ( `double`, solution 1       ) Y1 position in NDC coordinates
        - yhi            : ( `double`, solution 1       ) Y2 position in NDC coordinates
        - dright         : ( `double`, solution 2       ) Distance from right margin to right end of PaveText (text in PaveText can jut out)
        - dtop           : ( `double`, solution 2       ) Distance from top margin to upper end of PaveText
        - height         : ( `double`, solution 2       ) Height of PaveText in units of textsizes
        - header         : ( `string`, default `""`     ) Header of the PaveText
        - TextSize       : ( `double`, default `0.05`   ) Set the PaveTexts textsize
        - TextAlign      : ( `short` , default `12`     ) Align text https://root.cern.ch/doc/master/classTAttText.html#T1
        - TextAngle      : ( `float` , default `0.f`    ) Set text angle https://root.cern.ch/doc/master/classTAttText.html#T2
        - BorderSize     : ( `int`   , default `0`      ) Set width of line around PaveText
        - FillColor      : ( `string`, default `"white"`) Set fill color (needs to be set to change `FillColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - FillColorAlpha : ( `double`, default `1.0`     ) Set fill color alpha
        - FillStyle      : ( `int`   , default `1001`    ) Set fill style https://root.cern.ch/doc/master/classTAttFill.html#F2
        - AddText        : ( ptree child, optional       ) The objects in this child will be added as text to the PaveText <br>
        AddText can have special childs: "KSTest" and "Integral" (only well behaved for TH1s). They can be used like this:

                pavetexts {
                  pt1 {
                    xlo      0.18
                    dtop     0.29
                    height   1.0
                    dright   0.6
                    TextSize 0.06
                    AddText {
                      KSTest {
                        DATAHIST { ; name of 1st histogram (draw_stuff iterates within KSTest)
                          comparand "MCHIST"    ; mandatory: name of 2nd histogram
                          text      "KS Test #" ; mandatory: text which will be added to the pavetext. '#' is replaced by the Kolmogorov-Smirnov test score
                          precision 3           ; optional: floating point precision printed on the pavetext (default 6)
                          opt       "M"         ; optional: see https://root.cern.ch/doc/master/classTH1.html#a2747cabe9ebe61c2fdfd74ff307cef3a
                        }
                        NEXTDATAHISTINTHISPLOT {
                          ...
                        }
                      }
                      Integral {
                        ROC_CURVE_BDT { ; name of histo (draw_stuff iterates within Integral)
                          text      "Integral #"    ; mandatory: text which will be added to the pavetext. '#' is replaced by the Integral
                          precision 3               ; optional: floating point precision printed on the pavetext (default 6)
                          opt       "width"         ; optional: see https://root.cern.ch/doc/master/classTH1.html#ab039c428a41b1ab39bbd29b3c69a6b91
                          first_bin 1               ; optional: default 1
                          last_bin  42              ; optional: default TH1::GetNbinsX()*TH1::GetNbinsY()*TH1::GetNbinsZ()
                        }
                        ROC_CURVE_MLP {
                          ...
                        }
                      }
                    }
                  }
                }


#### Optional objects in `Plots`-iterator:
- drawing_opt      : ( `string`, default parsed     ) Draw plotable object with this option. Defaults: TH1-`"e1p"`,TF1-`"c"`,TGraph2D-`"cont4z"`,TGraph-`"ac"`,TEfficiency-`"ap"`,TMultigraph-`"ac"`
- FillColor        : ( `string`, default predefined ) Set fill color (needs to be set to change `FillColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
- FillColorAlpha   : ( `double`, default `1.0`      ) Set fill color alpha
- FillStyle        : ( `short` , default predefined ) Set fill style https://root.cern.ch/doc/master/classTAttFill.html#F2
- LineColor        : ( `string`, default predefined ) Set Line color (needs to be set to change `LineColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C` (for TH2A this will change the color of the bin-border lines)
- LineColorAlpha   : ( `double`, default `1.0`      ) Set Line color alpha
- LineWidth        : ( `short` , default predefined ) Set Line Width https://root.cern.ch/doc/master/classTAttLine.html#L2 (for TH2A this will change the with of the bin-border lines)
- LineStyle        : ( `short` , default predefined ) Set Line style https://root.cern.ch/doc/master/classTAttLine.html#L3
- MarkerColor      : ( `string`, default predefined ) Set Marker color (needs to be set to change `MarkerColorAlpha`) All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
- MarkerColorAlpha : ( `double`, default `1.0`      ) Set Marker color alpha
- MarkerStyle      : ( `short` , default predefined ) Set Marker style https://root.cern.ch/doc/master/classTAttMarker.html#L2
- MarkerSize       : ( `float` , default predefined ) Set Marker Width https://root.cern.ch/doc/master/classTAttMarker.html#L3
- Title            : ( `string`, default predefined ) Set Title https://root.cern.ch/doc/master/classTNamed.html#a72f4d419b48a5fbfef07485b335139ca
- Norm             : ( `bool`,   default false      ) Scale by 1/sum of weights
- Scale            : ( `double`, optional (TH1 only)) Scale TH1 by this factor
- Scale_to         : ( `string`, optional (TH1 only)) Scale this TH1 to a previously drawn one
- EndErrorSize     : ( `float` , default predefined ) Set EndErrorSize. Default is 3 times MarkerSize. See https://root.cern.ch/doc/master/classTStyle.html#a28c2a1879f54446fcda3c39707f84ea6
- DPBoundary       : ( ptree child, optional (TH2A) only ) Draw Dalitz plot boundary. See http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/classTH2A.html#afba6a3de93eafaffcd861142d48f7fd2
    - mother: ( `string`, default "Lambda_b0" ) Name of mother for DPBoundary
    - d1    : ( `string`, default "Lambda_c+" ) Name of first daughter for DPBoundary
    - d2    : ( `string`, default "D0"        ) Name of second daughter for DPBoundary
    - d3    : ( `string`, default "K-"        ) Name of third daughter for DPBoundary
    - nbins : ( `int`   , default 10e+4       ) Number of pixels with which the Dalitz plot boundary is computed

- *axes*  : Optional child to configure axes. `axes` has iterable objects, but only `X`, `Y`, `Z` and `PaletteAxis` are parsed.
    - LabelOffset : ( `float` , default predefined ) SetLabelOffset. See https://root.cern.ch/doc/master/classTAxis.html for this and the following options
    - LabelSize   : ( `float` , default predefined ) SetLabelSize
    - Ndivisions  : ( `int`   , default predefined ) SetNdivisions
    - TickLength  : ( `float` , default predefined ) SetTickLength
    - TickSize    : ( `float` , default `0.03`     ) SetTickSize
    - Title       : ( `string`, default predefined ) SetTitle
    - TitleOffset : ( `float` , default predefined ) SetTitleOffset
    - TitleSize   : ( `float` , default predefined ) SetTitleSize
    - min         : ( `float` , default optional   ) Set range minimum (uses SetRangeUser)
    - max         : ( `float` , default optional   ) Set range maximum
    - parse_range : ( `bool`  , default `false`    ) Allows to use `maxadd` and `minsub` options. Axis range gets parsed by calling the plot object's `GetMinimum()` and `GetMaximum()`. Log-scale plots are parsed such that maxadd and minsub are linear on the resulting plot scale
    - maxadd      : ( `float` , default `0.1`      ) Adds `maxadd*(max-min)` to parsed maximum (helpful to add space for big legends in a semi-automated way)
    - minsub      : ( `float` , default `0.0`      ) Subtracts `minsub*(ymax-ymin)` from parsed minimum
    - configurables in *PaletteAxis* : (see https://root.cern.ch/doc/master/classTPaletteAxis.html)
        - LabelColor  ( `string`, optional           ) Sets label color. All colors will be given as strings and parsed by `LHCb::color`, defined in `lhcbStyle.C`
        - LabelFont   ( `int`   , optional           ) Sets label font
        - LabelOffset ( `float` , optional           ) Sets label offset
        - margin      ( `double`, optional           ) Sets distance of left edge of PaletteAxis to plot-border
        - width       ( `double`, optional           ) Sets width of PaletteAxis
        - Y1NDC       ( `double`, default predefined ) Sets lower edge of PaletteAxis in NDC coordinates
        - Y2NDC       ( `double`, default predefined ) Sets upper edge of PaletteAxis in NDC coordinates

                axes {
                  X {
                    min 17e+6
                    Title "blah"
                  }
                  PaletteAxis {
                    Y1NDC      0.2
                  }
                }


### Config files without a Canvas iterator
This is designed for quick plotting of wildcarded objects in a single root file. A use case would be that a series of plots have been dumped in a root file and they all should be drawn with the same plotting style.<br>
It has basically the same functionality as described below, but currently, TPads and having several plots in a single canvas is not supported (unless you use `TMultigraph`)<br>
An example is given [here](config/draw_stuff_example_wildcard.info). It can not be used with a Canvas iterator in the same config file.<br>
#### Special configurables when not using a Canvas iterator:
Since the name of the plot will be constructed from the name of the object, `output_name_prefix` and `output_name_suffix` can be given.
The `name` field within `Plots` uses wildcards. If you use `name "*"`, every drawable object will be plotted. But also something like `name "stuff*2012*"` is valid and will only plot objects,
whose name contains `stuff` and `2012`

# CMake instructions for TH2A support
- The 2DAdaptiveBinning repository needs to be checked out <somewhere>
- The `TH2A` module needs to be created in the parent project. The most convenient way to do this, is to use the `TH2A` [find-module](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning/blob/master/cmakeModules/FindTH2A.cmake)
- For this, the environment variable TH2A_DIR needs to be set to <somewhere>
- To use the find module embed it in the parent CMakeLists.txt with e.g.
    ```
    message(STATUS "Setting up TH2A")
    list(APPEND CMAKE_MODULE_PATH "$ENV{TH2A_DIR}/cmakeModules")
    find_package(TH2A REQUIRED)#from ${CMAKE_MODULE_PATH}/FindTH2A.cmake
    target_link_libraries(${TH2A_MODULE} ${ROOT_LIBRARIES})#optional. works without in most cases, sometimes only the EG library is needed
    ```
- CMake needs to be run with the flag `-DWITH_TH2A_PLOTTING=ON` (or hardcode the flag in src/CMakeLists.txt)

