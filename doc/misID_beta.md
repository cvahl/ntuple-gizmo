# Purpose
Oversimplified description: This tool helps you to identify backgrounds due to misidentified particles. <br>
Only 3 steps needed (see below):
1. modify selection
2. make histograms
3. and plots (2. and 3. automated)
<br><br>

The script is used to add all possible generalised single particle momentum asymmetries
described in [LHCb-ANA-2016-004](https://cds.cern.ch/record/2121282?ln=en),
and the corresponding invariant masses for a given number of final state particles to the tuple. <br>
It does so by creating the relevant branches and functors when setting up the tree
(in `SlaveBegin` when using the `TSelector` framework), and evaluating the functors
when processing the chain/tree given a set of `TLorentzVector`s.

# Usage
To use the script, adding the following lines of code to your selection script is sufficient (modulo some includes):

```
#include "../externals/ntuple-gizmo/include/misID_beta.h"
...
//initialise values to fill tree and functors
std::vector<double> mbs;
std::vector<std::function<double(std::vector<TLorentzVector>&)>> bfs;
std::vector<TBranch*> beta_branches;//only needed when using a TSelector(?)
//see https://root.cern.ch/doc/master/classTTree.html Case E
...
//set up the branches and functor (4 in this case is the number of final state particles, outtree the tree)
beta_branches = misID_beta(4,mbs,outtree,bfs);
...
//in the event loop, do something like
std::vector<TLorentzVector> misID_lv = {std::move(Kp_Ds),std::move(Km_Ds),std::move(pi_Ds),std::move(pi)};
for(decltype(mbs.size()) ij = 0; ij < mbs.size(); ij++){
  mbs[ij] = bfs[ij](misID_lv);
  beta_branches[ij]->SetAddress(&mbs[ij]);//only needed when using a TSelector(?)
}
//... to evaluate the functors and assign the values to be stored in the tree
```

# Make histograms of all combinations
This step has been automatized. Just run the executable of this script: `src/make_misID_hists.cpp` <br>
The command line arguments are:
- `-d` working directory (files will be prepended with this directory, optional)
- `-i` input file
- `-o` output file
- `-t` `TTree` name
- `-v` verbosity (1-3, optional)
- `-h` help

The script will produce a `.root` file with a histogram for every possible (physically meaningful) combination.

# Plot all the histograms with the same style
For this task, one can use the [draw_stuff](draw_stuff.md) wildcard-function. See for example
[this](https://gitlab.cern.ch/lhcb-bandq-exotics/Lb2LcD0K/blob/master/config/plotting/drawMisIDPlots_LcD0K.info) config file
