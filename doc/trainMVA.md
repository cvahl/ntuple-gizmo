# Documentation for trainMVA
This script allows for training and testing a multi-variate classification or regression using TMVA and config files based on [the info parser of boost property trees](http://www.boost.org/doc/libs/1_63_0/doc/html/property_tree/parsers.html#property_tree.parsers.info_parser). <br>
The input-files and -variables as well as the settings for training and testing are configurable to easily allow for systematic studies.<br>
***ATTENTION*** This is the training script for root version 6.04.16. After 6.08, TMVA uses the `DataLoader` class that takes over the data handling functionality of the `Factory` class.
You can find a version of this script for 6.08 here: https://gitlab.cern.ch/mstahl/Lb2LcD0K/blob/ROOT608/src/trainMVA.cpp . The way to write the config file stays the same

## Arguments for the executable
- `-c` : config file
- `-d` : work directory (ouput file and TMVA's weight files will be created there)
- `-o` : ouput file name
- `-v` : verbosity (1-3, optional)

## Example config file used for DfromBBDTs
[B2D0Pi config](https://gitlab.cern.ch/sneubert/DfromBBDTs/blob/master/config/MVA/MVAconfigData_B2D0Pi.info)

# Writing a config file
Mandatory child:
- *tasks* : contains all the configuration needed for classification or regression. The name of the individual node is equal the name of the TMVA::Factory object. This name is used internally, so that it is part of the resulting weight file
    - *sig_filename* : ***mandatory*** root file containing signal data (data must be in a TTree within this file)
    - *bkg_filename* : ***mandatory*** root file containing background data (data must be in a TTree within this file)
    - *sig_treename* : ***mandatory*** name of signal tree
    - *bkg_treename* : ***mandatory*** name of background tree
    - *sig_friendfiles* : (optional) child containing friend file names (name of the individual nodes in *sig_friendfiles* is irrelevant)
        - *filename* : ***mandatory*** name of friend file
        - *treename* : ***mandatory*** name of friend tree
    - *bkg_friendfiles* : analogous to *sig_friendfiles*
    - *sig_weightname* : (optional) name of signal weight expression
    - *bkg_weightname* : (optional) name of background weight expression
    - *sig_cut* : (optional) cut as TCut to be applied on the signal tree
    - *bkg_cut* : (optional) cut as TCut to be applied on the background tree
    - *variables* : ***mandatory*** child containing variable configuration. The name of the individual node is equal to name of the variable.
    (Since the structure of this child is the same as in [beef](https://gitlab.cern.ch/sneubert/beef/blob/master/config/README_workspace_config.md), variables can be written to a config file that is included here and in createWorkspace)
        - *label* : (optional, default is the name of the variable) title in https://root.cern.ch/doc/v606/classTMVA_1_1Factory.html#a13d6126e2da9401420b9bf1d8d11b14d
        - *unit* : (optional, default blank) unit in https://root.cern.ch/doc/v606/classTMVA_1_1Factory.html#a13d6126e2da9401420b9bf1d8d11b14d
        - *type* : (optional, default `F`) type in https://root.cern.ch/doc/v606/classTMVA_1_1Factory.html#a13d6126e2da9401420b9bf1d8d11b14d
    - *spectators* : (optional) child containing spectator configuration. The name of the individual node is equal to name of the variable.
        - *label* : (optional, default is the name of the variable) title
        - *unit* : (optional, default blank) unit
        - *low* : ***mandatory*** lower limit
        - *high* : ***mandatory*** upper limit
    - *factory_option* : ***mandatory*** option string passed to the TMVA::Factory object. See option table 1 (p. 15) of the [TMVA documentation](http://tmva.sourceforge.net/docu/TMVAUsersGuide.pdf)
    - *traintestopt* : ***mandatory*** option string to prepare testing and training. See 3.1.4 Preparing the training and test data in the [TMVA documentation](http://tmva.sourceforge.net/docu/TMVAUsersGuide.pdf)
    - *method* : ***mandatory*** child containing the training configuration (the name of this node will be used by TMVA internally and ends up as part of the weight-file and in the .root output file)
        - *type* : ***mandatory*** integer to decode the TMVA::Types::EMVA enum, see https://root.cern.ch/doc/v606/classTMVA_1_1Types.html (be aware that the enum changed with root version!!!)
        - *option* : ***mandatory*** string to configure the training. See option tables of various methods in the [TMVA documentation](http://tmva.sourceforge.net/docu/TMVAUsersGuide.pdf) and the latest [TMVA doxygen docu](https://root.cern.ch/doc/master/namespaceTMVA.html)
