# Documentation for tree_trimmer.cpp

The script is basically a implementation of `TTree::CopyTree()` on a `TEntryList` that can handle friend-trees.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file
- `-o` : output file
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile:friendtree` combinations

## The config file
The following can be given in a config file to tree_trimmer:

- basiccuts : ( `string`, default `""`   )  Cut with wich the dataset will be read in (`TCut` format)

- variables: Variables which are saved in the ouput tree. <br>
  Their name is given as object identifier, ***mandatory*** parameters are `low`, `high` to specify the range. <br>
  **ATTENTION** be aware that the given range is an implicit cut on the data

- noImplicitCuts : optional. If given the ranges will be ignored

- outtreename: Name of the tree in the output file (it's the inputname by default)

### Special commands to manipulate variables
Variables can be renamed and/or transformed easily by the `new_name` key in the `variables`-list.
Currently, the following transformations are parsed: `log_` (log10(x)), `sqrt_`, `atan_` and `NNtfd_` (log10(x/(1-x))).

### A full example:

        basiccuts "K_ProbNNk > 0.01 && D0BDT > -0.03 && LcBDT > -0.03"

        variables {
            Lb_PT {  ;1st item in variables-list
              low  2
              high 5 ;the implicit cuts apply to the tansformed variables
              new_name "log_Lb_pt"
            }
            Lb_ETA { ;2nd item in variables-list
              low 1.9
              high 5.5
            }
        }
