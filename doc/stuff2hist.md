# stuff2hist
`stuff2hist` is a small script to save histograms from an `TTree`, `TChain` or a  `RooDataSet` in a root file.
The script is able to deduce the input.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file or chain (a chain is parsed by giving a wildcard `*` in the filename)
- `-o` : output file
- `-t` : `TTree` or `RooWorkspace` name
- `-r` : `RooDataSet` name (optional)
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile:friendtree` combinations

## TTree, RooWorkspace/RooDataSet or TChain
Depending on the command-line options, `stuff2hist` will decide which of the 3 options is used. <br>
Note that transformations like `log10(var)` are only possible with `TTree`s and `TChain`s
- The most simple case is making histograms from a `TTree`, where `-i` is a single `.root` input file and `-t` is the name of the `TTree`.
- A `RooDataSet` has to live within a `RooWorkspace` for `stuff2hist`. `-i` is again a single `.root` input file, 
  but `-t` is the name of the `RooWorkspace`. To find the `RooDataSet`, `-r` has to be filled with it's name (default is "ds").
- A TChain is parsed from the `-i` command line option. Either a wildcard `*` is given in the filename, or `-i Chain` is used. 
  In the latter case, `stuff2hist` expects a `files` node in the config file with the list of files as key. Wildcards may be used in the node. An example is given below. 
  The `-t` command line option expects the name of the `TTree` within each `.root` file of the chain.

# Write a config file
Internally the script uses the `Draw()` function of `TTree` or `TChain` or the `createHistogram()` function of `RooAbsData`. So the histograms to be saved are configured by either:
- Draw, cut, opt, nentries and firstentry to mimic the `Draw()` function (only Draw is mandatory)
- or var, nbins, min, max, yvar, nbinsy, ymin, ymax, zvar, nbinsz, zmin, zmax and histname to have a easyly understandable configuration. Here only var is mandatory

## Weights
Weights have to be handled differently for `RooDataSet` and `TTree`/`TChain`. When making histograms from a weighted `RooDataSet`, the name of the weight is given by "weight" in the top level of the configuration or alternatively for each histogram to allow drawing of weighted and unweighted datasets with the same config file.
Weights in `TTree` or `TChain` are handled by "cut" as it is known from the `Draw()` function.


## Examples
Using a weighted `RooDataSet`:

```
hists {; hists has to be given
  hist1 {; this will be iterated over, the name doesn't matter
    ;Draw "log10_Lb_Cons_BPVIPCHI2>>log10_Lb_Cons_BPVIPCHI2(40,-3.5,1.5)" ; this is equivalent to the 4 lines below
    var   "log10_Lb_Cons_BPVIPCHI2"
    nbins "40"  ; note that numbers are given as strings
    min   "-3.5"
    max   "1.5"
  }
  hist3D {
    Draw    "log10_D0_Lb_DIRAQ:log10_Lb_Cons_Lc_BPVIPCHI2:Lc_ctauSign_tfd>>mycrazy3Dhist(40,-25.0,6.0,40,-1.0,4.4,40,-1.7,1.7) ; or
    ;var    "log10_D0_Lb_DIRAQ"
    ;nbins  "40"
    ;min    "-25.0"
    ;max    "6.0"
    ;yvar   "log10_Lb_Cons_Lc_BPVIPCHI2"
    ;nbinsy "40"
    ;ymin   "-1.0"
    ;ymax   "4.4"
    ;zvar   "Lc_ctauSign_tfd"
    ;nbinsz "40"
    ;zmin   "-1.7"
    ;zmax   "1.7"
  }
}
weight "NLb_sw";
```

Using a weighted `TTree` or `TChain`:

```
hists {
  hist1 {
    Draw "log10_Lb_Cons_BPVIPCHI2:Lc_ctauSign_tfd>>blub2D(40,-3.5,1.5,40,-1.7,1.7)"
    cut  "NLb_sw*(abs(mLb-5.62097e+03) < 2*7.48986)" ;of course sweights are not normalised any more when applying a cut
    opt  "COLZ"
  }
}
```

Use "per-histogram" weights with a `RooDataSet`:

```
hists {
  charm2D {
    Draw   "mLc:mD0>>charm2D(49,1817,1915,35,2252,2322)"
    cut    "abs(mLb-5.62097e+03) < 2*7.48986"
  }
  charm2D_weighted {
    Draw   "mLc:mD0>>charm2D_weighted(49,1817,1915,35,2252,2322)"
    cut    "abs(mLb-5.62097e+03) < 2*7.48986"
    weight "NLb_sw"
  }
}
```

Config file for a TChain using RunI data only:

```
hists {
  mLb {
    Draw "Lb_Cons_M>>mLb(125,5350,5850)"
  }
}
files {
  root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/ntuples/bdt_LcD0K_2012*.root
  root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/ntuples/bdt_LcD0K_2011*.root
}
```
