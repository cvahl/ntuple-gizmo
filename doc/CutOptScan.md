# Documentation for CutOptScan
CutOptScan is a script to optimise simple cuts in [optionally k-folds](https://en.wikipedia.org/wiki/Cross-validation_(statistics)#k-fold_cross-validation) for a set of variables. <br>
It does so by scanning through a (configurable) range of cuts and maximising a (configurable) figure of merit. <br>
To obtain this figure of merit a fit is done at each "point" of the scan and the number of signal and background events in a configurable range of the observable is calculated.<br>
The scan is entirely configurable and can be done in several steps.
Each variable can be scanned separately (1D scan), as part of the full phase space spanned by the set of variables (full scan), or a combination of both.<br>
Pre-configured or previously found cuts on the other variables are taken into account during the scan.
The best cuts will be written as TCut to an output file, or - in the case of k-fold cross validation to a file containing one cut per line.
The resulting k files can later be combined and appended to existing cut-config files with [CombineBestCuts.cpp](scripts/Selection/CombineBestCuts.cpp).<br>

## Arguments for the executable
- `-c` : config file
- `-d` : work directory
- `-i` : input file (with a RooWorkspace and RooDataSet(s))
- `-o` : ouput file name (without file name extension - .info will be attached. If fold number is given, it will be added as suffix to the name)
- `-r` : name of RooDataSet (Default "ds". If fold number is given, it will be added as suffix to the name)
- `-w` : name of RooWorkspace (Default "w")
- `-v` : verbosity level (3 (DEBUG) will print results for FoM at ech point, 2 only prints essential infos (final results, parameter hit range limit))
- nonoptions : fold number (optional)

## Config file used in this analysis
[CutOptScan.info](config/MVA/CutOptScan.info)

## Writing a config file
Mandatory:
- *beef_config* : location of [config file for beef](https://gitlab.cern.ch/sneubert/beef/blob/master/config/README_fit_config.md), as beef is called internally at every point of the scan
- *sig_pdf* : signal pdf as given in beef_config file
- *bkg_pdf* : background pdf as given in beef_config file
- *Nsig_var* : extended parameter of signal pdf (name of fitparameter that gives number of signal events)
- *Nbkg_var* : extended parameter of background pdf (name of fitparameter that gives number of background events)

one of the following set of configurables:
- *sig_range_lo*
- *sig_range_hi* : to calculate the FoM in [sig_range_lo,sig_range_hi]
or
- *nsigma* : to calculate the FoM in [mean_var - nsigma*width_var, mean_var + nsigma*width_var]. To use this, the following is needed as well:
- *width_var* : parameter of signal pdf correspondig to signal width (a unit of this variable should correspond to the width of a normal distribution)
- *mean_var* : parameter of signal pdf correspondig to signal median


optional:
- *suppress_RootInfo* : only print messages from warning level in root and roofit (note that beef has it's own settings in *beef_config*)
- *FoM_mode* : currently only 0 (S/sqrt(S+B)) or 1 (S/sqrt(S+B)*S/(S+B)) (default 0)
- *nsteps_1D* : number of scan steps (the scan will "zoom" into the region with the highest FoM)
- *nsteps* : see above

children:
- *vars* : mandatory child node that contains the variables that will be scanned
    - nodename in vars   : name of the variable
    - *operator* : default ">", defines the cut operation
    - *loosest_cut* : ***mandatory*** , defines loosest possible cut (the scan will not go looser than this cut)
    - *cut_distance_1D* : initial step size between cuts for the 1D scan (default 0.0 --> the 1D scan will be skipped)
    - *ncuts_1D* : initial number of cuts for this variable in the 1D scan (default 5, will be extended if the best FoM was found at the edge of the scan-range spanned by [start_cut_1D, start_cut_1D + (ncuts_1D-1)*cut_distance_1D])
    - *start_cut_1D* : initial cut for this variable in the 1D scan. the scan will start at this point (default *loosest_cut*)
    - *nintermediate_1D* : number of steps that will be inserted between the best cut that was found and it's neighboring cuts for the next step of the scan (default 1 --> so {best-1,best,best+1} will become {loosest( == best - 1),intermediate,(old) best,intermediate,tightest( == best + 1)})
    - *cut_distance* : step size between cuts for the full scan (default is given from last step of 1D scan, fallback to *cut_distance_1D* if there was no 1D scan)
    - *ncuts* : number of cuts for this variable in the full scan (default 5, cut will be fixed if *ncuts* < 3)
    - *start_cut* : initial cut for this variable in the full scan. the scan will start at this point (default: current loosest cut as determined from the last step of the 1D scan)
    - *nintermediate* : see 1D description

